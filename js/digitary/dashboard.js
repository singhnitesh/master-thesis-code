/**
 * Created by Digitay Development Team: Siddhartha Moitra, Nitesh Singh on 18.04.16.
 */

function add_email_account() {
    var emailAddress = $('#email-address').val().trim();
    if(emailAddress.endsWith('@gmail.com')) {
        var indexOfAt = emailAddress.indexOf('@');
        emailAddress = emailAddress.substring(0, indexOfAt);
    }
    // API call to get google URL
    get_email_request_auth(emailAddress, function(data) {
        if (data instanceof DigitaryWebError) {
            // TODO Something
        } else {
            //window.location.href = data.authRequestURL;
            window.open(data.authRequestURL, "popupWindow", "width=800,height=600,scrollbars=no");
        }
    });
}

function refresh_email_ui() {
    var html = "<div class='text-center' style='height: 80%;'><h3><i class='fa fa-cog fa-spin'></i> Requesting Gmail API for all emails data... </h3></div>";
    $('#email').empty();
    $('#email').append(html);

    var cookie_uid = $.cookie(COOKIE_UID);
    // Create a map of URL resources
    var map = new Map();
    map.set("{userID}", cookie_uid);

    get_user_profile_call(map, true, function (usr) {
        if (usr instanceof DigitaryWebError) {
            // TODO Show something
        } else {
            if(usr.userEmailList.length>0) {
                load_email_data(usr.userEmailList[0]);
            }
        }
    });
}
//add initial taskgroup
function add_taskgroup_name() {
    var taskGroupName = $('#taskgroup-name').val().trim();
    var newTaskGroup = new Object();
    newTaskGroup.taskGroupName = taskGroupName;
    // API call to get google URL
    create_task_group_call(newTaskGroup, true, function(taskGroup) {
        if (taskGroup instanceof DigitaryWebError) {
            // TODO Something
            console.log("TaskGroup can not created");
        } else {
            //console.log("Taskgroup successfully created");
            $('#taskGroup-name-added').modal('show');
        }
    });
}
function refresh_taskGroup_ui() {
    var html = "<div class='text-center' style='height: 80%;'><h3><i class='fa fa-cog fa-spin'></i> Requesting TaskGroup API for all task... </h3></div>";
    $('#task').empty();
    $('#task').append(html);

    var cookie_uid = $.cookie(COOKIE_UID);
    // Create a map of URL resources
    var map = new Map();
    map.set("{userID}", cookie_uid);

    get_user_profile_call(map, true, function (usr) {
        if (usr instanceof DigitaryWebError) {
            // TODO Show something
        } else {
            if(usr.taskGroupList.length>0) {
                load_task_groups(usr.taskGroupList[0]);
            }
        }
    });
}
//add initial NOTEBOOK
function add_notebook_name() {
    var notebookName = $('#notebook-name').val().trim();
    var newNotebook = new Object();
    newNotebook.notebookName = notebookName;
    // API call to get google URL
    create_notebook_call(newNotebook, true, function(notebook) {
        if (notebook instanceof DigitaryWebError) {
            // TODO Something
            console.log("Notebook can not created");
        } else {
            //console.log("Notebook successfully created");
            $('#notebook-name-added').modal('show');
        }
    });
}
function refresh_notebook_ui() {
    var html = "<div class='text-center' style='height: 80%;'><h3><i class='fa fa-cog fa-spin'></i> Requesting Notebook API for all notes... </h3></div>";
    $('#notebook').empty();
    $('#notebook').append(html);

    var cookie_uid = $.cookie(COOKIE_UID);
    // Create a map of URL resources
    var map = new Map();
    map.set("{userID}", cookie_uid);

    get_user_profile_call(map, true, function (usr) {
        if (usr instanceof DigitaryWebError) {
            // TODO Show something
        } else {
            if(usr.notebookList.length>0) {
                load_notebook_hierarchy(usr.notebookList[0]);
            }
        }
    });
}
function show_modal_edit_notebooks() {
    if($('#edit-notebooks').length == 0) {
        // Now we load the underscore template
        var templateData = new Object();
        var html_template = get_inner_HTML("templates/modals/edit-notebooks.html");
        var template = _.template(html_template);
        $('#modal-section').append(template(templateData));
        init_edit_notebooks();
    }
    $('#edit-notebooks').modal('show');
}

//modal edit for task group
function show_modal_edit_task_groups() {
    if($('#edit-task-groups').length == 0) {
        // Now we load the underscore template
        var templateData = new Object();
        var html_template = get_inner_HTML("templates/modals/edit-task-groups.html");
        var template = _.template(html_template);
        $('#modal-section').append(template(templateData));
        init_edit_task_groups();
    }
    $('#edit-task-groups').modal('show');
}
//modal to create new label
function show_modal_create_new_label() {
    if($('#create-label').length == 0) {
        var templateData = new Object();
        var html_template = get_inner_HTML("templates/labelsearch/create-label.html");
        var template = _.template(html_template);
        $('#modal-section').append(template(templateData));
        init_label_create();
    }
    $('#create-label').modal('show');
}

//load task groups
function load_task_groups(taskGroupID) {
    var map = new Map();
    map.set("{taskGroupID}", taskGroupID);
    get_task_group_call(map, true, function (taskGroup) {
        if (taskGroup instanceof DigitaryWebError) {
            // TODO Something
        } else {
            // I got TASK GROUP hierarchy
            USR_PROFILE.addTaskGroup(taskGroup);
            var map = new Map();
            map.set("{taskGroupID}", taskGroup.id);
            // Finally we can start creating UI for taskgroup-content
            get_tasks_call(map, true, function(tasks){
                if (tasks instanceof DigitaryWebError) {
                    // TODO Something
                } else {
                    var templateData = new Object();
                    templateData.tasks = tasks;
                    // Load the html template file
                    var html_template = get_inner_HTML("templates/task/task-base.html");
                    var template = _.template(html_template);
                    $('#task').empty();
                    $('#task').append(template(templateData));

                    $("#select-task-group").val(taskGroup.taskGroupName);
                    $("#tGroupID").val(taskGroup.id);

                    init_task_base();
                }
            });

        }
    });
}
function load_notebook_hierarchy(notebookID) {
    var map = new Map();
    map.set("{notebookID}", notebookID);
    get_notebook_hierarchy_call(map, true, function (notebook) {
        if (notebook instanceof DigitaryWebError) {
            // TODO Something
        } else {
            // I got notebook hierarchy
            USR_PROFILE.addNotebook(notebook);
            // Finally we can start creating UI for notebook-content
            load_notebook_ui(USR_PROFILE.getNotebook(notebook.id));
        }
    });
}
function load_email_data(emailAccountID) {
    var map = new Map();
    map.set("{accountID}", emailAccountID);
    get_email_call(map, true, EMAIL_ALL_INBOX, function(emails) {
        if (emails instanceof DigitaryWebError) {
            // TODO Something
            show_error_message("Could not retrieve emails.",emails);
        } else {
            var templateData = new Object();
            templateData.emails = emails;
            // Load the html template file
            var html_template = get_inner_HTML("templates/email/email-base.html");
            var template = _.template(html_template);
            $('#email').empty();
            $('#email').append(template(templateData));
            init_email_base(emails);
        }
    });
}
function load_labels() {
    get_label_call(function(labels) {
        if (labels instanceof DigitaryWebError) {
            // TODO Do something
        } else {

            $.each(labels, function(index, label) {
                USR_PROFILE.addLabel(label);
            });
            var templateData = new Object();
            templateData.labels = labels;
            // Load the html template file
            var html_template = get_inner_HTML("templates/labelsearch/search-by-labels.html");
            var template = _.template(html_template);
            $('#label').empty();
            $('#label').append(template(templateData));
        }
    });
}

function load_user_profile() {
    var cookie_uid = $.cookie(COOKIE_UID);
    if (cookie_uid && cookie_uid.length > 0) {
        // Create a map of URL resources
        var map = new Map();
        //map.set("{username}", cookie_uid);
        map.set("{userID}", cookie_uid);

        get_user_profile_call(map, true, function (usr) {
            if (usr instanceof DigitaryWebError) {
                // TODO Show something
            } else {
                // I will create a user profile to store.
                USR_PROFILE = new User(usr);
                // I will fetch labels
                load_labels();
                // Now I will fetch everything
                // I also know only 1 notebook, 1 taskgroup and 1 email account present
                // For the moment: demo purpose
                var notebookID;
                var taskGroupID;
                var emailAccountID;

                if(usr.notebookList.length>0) {
                    notebookID = usr.notebookList[0];
                    load_notebook_hierarchy(notebookID);
                }else {
                    // Load add email page
                    var templateData = new Object();
                    // Load the html template file
                    var html_template = get_inner_HTML("templates/notebook/add-notebook-init.html");
                    var template = _.template(html_template);
                    $('#notebook').empty();
                    $('#notebook').append(template(templateData));
                }
                if(usr.taskGroupList.length>0) {
                    taskGroupID = usr.taskGroupList[0];
                    load_task_groups(taskGroupID);
                } else {
                    // Load add email page
                    var templateData = new Object();
                    // Load the html template file
                    var html_template = get_inner_HTML("templates/task/add-taskgroup-init.html");
                    var template = _.template(html_template);
                    $('#task').empty();
                    $('#task').append(template(templateData));
                }
                if(usr.userEmailList.length>0) {
                    emailAccountID = usr.userEmailList[0];
                    load_email_data(emailAccountID);
                } else {
                    // Load add email page
                    var templateData = new Object();
                    // Load the html template file
                    var html_template = get_inner_HTML("templates/email/add-email-init.html");
                    var template = _.template(html_template);
                    $('#email').empty();
                    $('#email').append(template(templateData));
                }
            }
        });
    }
}

function fn_add_nav_handlers() {
    $("#lnk-logOut").click(function (e) {
        try {
            var obj = get_user_logout_call({}, false);
            // Delete current cookie and replace it with default value
            $.removeCookie(COOKIE_SID);
            $.removeCookie(COOKIE_UID);
            window.location.replace(APP_ROOT);
        } catch (err) {
            // TODO Show a modal error message
            var title = "Sign out failed";
            var content = "<div id='msgTitle'>Oops " + User + "... It seems we ran into problems.</div>" +
                "<div id='msgDetail'>Unfortunately, we cannot log you out.</div>"
        }
    });
}
//delete user profile confirmation modal
function confirmation_modal(){
    $('#user-profile-delete-alert').modal('show');
}
//delete user profile
function delete_user_profile(){
    //$("#lnk-delete-user-profile").click(function (e) {
        var cookie_uid = $.cookie(COOKIE_UID);
        // Create a map of URL resources
        var map = new Map();
        map.set("{userID}", cookie_uid);

        //API call to delete user profile
        delete_user_profile_call(map, true, function(usr) {
            if (usr instanceof DigitaryWebError) {
                // TODO Show something
            } else {
                $('#delete-userProfile').modal('show');
            }
        });
    //});
}
//Change location to home page
function go_home(){
    window.location.replace(APP_ROOT);
}
function add_event_handlers_dashboard() {
    fn_add_nav_handlers();
}

$(document).ready(function () {
    // When rending an underscore template, we want top-level
    // variables to be referenced as part of an object
    _.templateSettings.variable = "rc";
    // 2. Now load user profile
    load_user_profile();

    add_event_handler_left_view();
});

/*
window.setInterval(function(){
    /// call your function here

    load_new_email_inbox(USR_PROFILE.getEmailAccounts()[0]);
}, 5000);*/
