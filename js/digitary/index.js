/**
 * Scripts to validate and process information from homepage
 * Created by Digitay Development Team: Siddhartha Moitra, Nitesh Singh
 */

// Section 1. All related to login user
function get_user_login() {
    var username = $('#login-username').val();
    var password = $('#login-password').val();
    // Convert Password into SHA3 Hash
    var hashed_password = CryptoJS.SHA1(password).toString();
    // Calculate Base64 encoding
    var base64 = (username + ':' + hashed_password).toString(CryptoJS.enc.Base64);
    // Send AJAX request to login
    get_user_login_call(window.btoa(base64), false, function (user_access) {

        if (user_access instanceof DigitaryWebError) {
            window.location.replace(APP_ROOT + "login.html?error=" + encodeURIComponent(user_access.error.apiError) + "&cause=" + encodeURIComponent(user_access.error.causeMessage));
        } else {
            // Store access token received as cookie
            $.cookie(COOKIE_SID, user_access.accessToken, {path: '/', expires: 3650});
            //cookies.set(COOKIE_UID, username, {path: '/', expires: cookieExpire});
            console.log(user_access.userID);
            $.cookie(COOKIE_UID, user_access.userID, {path: '/', expires: 3650});
            // Redirect to dashboard
            window.location.replace(APP_ROOT + "dashboard.html");
        }
    });
}

var user_registration_data = {
    firstName: "",
    lastName: "",
    userName: "",
    password: "",
    confirm_password: "",
    createdDate: 0
};
// Section 2. All related to user registration
function update_btn_signup_state() {
    if (user_registration_data.firstName == '' ||
        user_registration_data.lastName == '' ||
        user_registration_data.userName == '' ||
        user_registration_data.password == '' ||
        user_registration_data.confirm_password == '') {
        $('#btn-signup').addClass('disabled');
    } else {
        $('#btn-signup').removeClass('disabled');
    }
}

$.fn.remove_old_feedback = function () {
    this.removeClass('error');
    this.parent().removeClass("has-error has-feedback");
    this.parent().removeClass("has-success has-feedback");
    this.parent().find('span').remove();
}
$.fn.update_input_after_validation = function (isValid, msg) {
    //$(this).remove_old_feedback();

    if (Boolean(isValid)) {
        this.parent().addClass("has-success has-feedback");
        var icon = "<span class='glyphicon glyphicon-ok form-control-feedback'></span>";
        $(this).after(icon);
    } else {
        this.addClass('error');
        this.parent().addClass("has-error has-feedback");
        var icon = "<span class='glyphicon glyphicon-remove form-control-feedback'></span>";
        $(this).after(icon);
        //error tooltip on registration page
        $(this).attr('title', msg);
        $(this).tooltip('show');
        $(this).click(function () {
            $(this).tooltip('destroy');
        });
    }
    update_btn_signup_state();
};

/**
 *
 * @param username
 * @returns {Boolean}
 */
function check_username_availability(username) {
    var returnValue = new Boolean(false);
    // Create a var of URL resources with their values
    var map = new Map();
    map.set("{username}", username);
    // Check if this username is available
    get_username_availability_call(map, false, function (user_availability) {
        if (user_availability instanceof DigitaryWebError) {
            // TODO Do something
            show_error_message("Username not present.",user_availability);
        } else {
            returnValue = Boolean(user_availability.available);
        }
    });

    return returnValue;
}

function get_user_registration() {
    // Delete the confirm password key from user_registration_data
    delete user_registration_data.confirm_password;
    // Convert Password into SHA3 Hash
    user_registration_data.password = CryptoJS.SHA1(user_registration_data.password).toString();
    // Calculate current time
    user_registration_data.createdDate = (new Date).getTime();

    var obj = post_user_registration_call(user_registration_data, false, function (data) {
        // First remove all the data and then see what we got
        // Always make the registration form back to initial state
        $('#firstname').val('');
        $('#firstname').remove_old_feedback();
        $('#lastname').val('');
        $('#lastname').remove_old_feedback();
        $('#username').val('');
        $('#username').remove_old_feedback();
        $('#password').val('');
        $('#password').remove_old_feedback();
        $('#confirmpassword').val('');
        $('#confirmpassword').remove_old_feedback();
        $('#btn-signup').addClass('disabled');
        // It is important to make user_registration_data back to initial state
        // Else the submit button will stay enabled: See fn update_btn_signup_state()
        user_registration_data.firstName = '';
        user_registration_data.lastName = '';
        user_registration_data.userName = '';
        user_registration_data.password = '';
        user_registration_data.confirm_password = '';

        if (data instanceof DigitaryWebError) {
            // TODO Create a modal
            var title = "Your account registration";
            var content = "<div id='msgTitle'>Oops " + user_registration_data.firstName + "... It seems we ran into problems.</div>" +
                "<div id='msgDetail'>Trust us, we got this issue covered for you.</div>"
            show_error_message("Could not register user.",data);
        } else {
            // TODO Show modal
            $('#registration-successful').modal('show');
            /*var title = "Your account registration";
             var content = "<div id='msgTitle'>Well " + user_registration_data.firstName + "... Things look great !!!</div>" +
             "<div id='msgDetail'>Please login with your credentials to ease your day with <b>Digitary</b>.</div>";*/
            //$('#login-username').focus();
        }
    });
}

function fn_add_validation_for_user_registration() {
    // 1. Validate First name
    $('#firstname').focus(function () {
        $(this).remove_old_feedback();
    });
    $('#firstname').focusout(function () {
        var f_name = $(this).val();
        if (!f_name.match('^[a-zA-Z]+$')) {
            $(this).update_input_after_validation(false, 'First Name is not valid.');
        } else {
            user_registration_data.firstName = f_name;
            $(this).update_input_after_validation(true, '');
        }
    });
    // 2. Validate Last name
    $('#lastname').focus(function () {
        $(this).remove_old_feedback();
    });
    $('#lastname').focusout(function () {
        var l_name = $(this).val();
        if (!l_name.match('^[a-zA-Z]+$')) {
            $(this).update_input_after_validation(false, 'Last Name is not valid.');
        } else {
            user_registration_data.lastName = l_name;
            $(this).update_input_after_validation(true, '');
        }
    });
    // 3. Validate User name
    $('#username').focus(function () {
        $(this).remove_old_feedback();
    });
    $('#username').focusout(function () {
        var u_name = $(this).val();
        if (!u_name.match('^([a-zA-Z]+[0-9]*){3,15}$')) {
            $(this).update_input_after_validation(false, 'Username is not valid. It must contain 3 characters and should not be more than 15 characters.');
        } else {
            var isUsernameAvailable = check_username_availability(u_name);
            if (isUsernameAvailable == false) {
                $(this).update_input_after_validation(false, 'Username is not valid.');
            } else {
                user_registration_data.userName = u_name;
                $(this).update_input_after_validation(true, '');
            }
        }
    });
    // 4. Validate Password
    $('#password').focus(function () {
        $(this).remove_old_feedback();
    });
    $('#password').focusout(function () {
        var password = $(this).val();
        var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})");
        if (!strongRegex.test(password)) {
            $(this).update_input_after_validation(false, 'Password is not valid. Password must be at least 6 characters, including at least 1 number and includes both lower and uppercase letters');
        } else {
            if (user_registration_data.confirm_password != '' && password != user_registration_data.confirm_password) {
                $(this).update_input_after_validation(false, 'Password and Confirm Password do not match');
            } else {
                user_registration_data.password = password;
                $(this).update_input_after_validation(true, '');
            }
        }
    });

    // 5. Validate Confirm Password
    $('#confirmpassword').focus(function () {
        $(this).remove_old_feedback();
    });
    $('#confirmpassword').focusout(function () {
        var confirmpassword = $(this).val();
        if (confirmpassword != user_registration_data.password || confirmpassword == '') {
            $(this).update_input_after_validation(false, 'Password and Confirm Password do not match.');
        } else {
            user_registration_data.confirm_password = confirmpassword;
            $(this).update_input_after_validation(true, '');
        }
    });
}

$(document).ready(function () {
    // Load initial UI contraints
    // 0. Disable signup button
    $('#btn-signup').addClass('disabled');
    // 6. Add action for submit button
    // On click even sends AJAX request to register user
    $("#btn-signup").on('click', function (e) {
        e.preventDefault();
        get_user_registration();
    });
    fn_add_validation_for_user_registration();
});
