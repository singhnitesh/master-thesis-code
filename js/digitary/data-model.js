function comparator_sort(a, b){
    return ((a.createdDate < b.createdDate) ? -1 : ((a.createdDate > b.createdDate) ? 1 : 0));
}
/**
 * Created by Digitay Development Team: Siddhartha Moitra, Nitesh Singh on 26.04.16.
 */
/*************************************************************
 * LABEL DATA MODEL
 ************************************************************/
var Label = function(label) {
    this.id = label.id;
    this.createdTime = label.createdTime;
    this.labelName = label.labelName;
}
/*************************************************************
 * TASK GROUP DATA MODEL
 ************************************************************/
var TaskGroup = function (taskGroup) {
    this.id = taskGroup.id;
    this.taskGroupName = taskGroup.taskGroupName;
    this.createdTime = new Number(taskGroup.createdTime);
}
/*************************************************************
 * CALENDAR EVENT DATA MODEL
 ************************************************************/
var Event = function (event) {
    this.id = event.id;
    this.name = event.name;
    this.createdDate = event.createdDate;
    this.startDate = event.startDate;
    this.endDate = event.endDate;
    this.detail = event.detail;
}

var EventContainer = function(events){
    this.createdEventArray = [];

    for(i=0; i<events.length; i++){
        var event = new Event(events[i]);
            this.createdEventArray.push(event);
    }
}

/*************************************************************
 * NOTEBOOK SECTION PAGE DATA MODEL
 ************************************************************/
var NotebookSectionPage = function (page) {
    this.id = page.id;
    this.pageName = page.pageName;
    this.createdTime = new Number(page.createdTime);
}

/*************************************************************
 * NOTEBOOK SECTION DATA MODEL
 ************************************************************/
var NotebookSection = function (section) {
    this.id = section.id;
    this.sectionName = section.sectionName;
    this.createdTime = new Number(section.createdTime);
    this.notebookSectionPageArray = [];
    for (i = 0; i < section.pageList.length; i++) {
        this.notebookSectionPageArray.push(new NotebookSectionPage(section.pageList[i]));
    }
}
NotebookSection.prototype.getPage = function (pageID) {
    for (i = 0; i < this.notebookSectionPageArray.length; i++) {
        if (pageID == this.notebookSectionPageArray[i].id) {
            return this.notebookSectionPageArray[i];
        }
    }
    return null;
}
NotebookSection.prototype.getPages = function () {
    var xpath = [];
    for (i = 0; i < this.notebookSectionPageArray.length; i++) {
        xpath.push(this.id + '.' + this.notebookSectionPageArray[i].id);
    }

    return xpath;
}
NotebookSection.prototype.addPage = function (page) {
    if (!this.getPage(page.id)) {
        var page_obj = new NotebookSectionPage(page);
        this.notebookSectionPageArray.push(page_obj);
    }
}
NotebookSection.prototype.updatePage = function (page) {
    for (i = 0; i < this.notebookSectionPageArray.length; i++) {
        if (page.id == this.notebookSectionPageArray[i].id) {
            this.notebookSectionPageArray[i].pageName = page.pageName;
            return;
        }
    }
}
NotebookSection.prototype.deletePage = function (pageID) {
    for (i = 0; i < this.notebookSectionPageArray.length; i++) {
        if (pageID == this.notebookSectionPageArray[i].id) {
            this.notebookSectionPageArray.splice(i, 1);
            return;
        }
    }
}
NotebookSection.prototype.sortPages = function(isRecursive) {
    if (isRecursive) {
        this.notebookSectionPageArray.sort(comparator_sort);
    }
}
/*************************************************************
 * NOTEBOOK DATA MODEL
 ************************************************************/
var Notebook = function (notebook) {
    this.id = notebook.id;
    this.notebookName = notebook.notebookName;
    this.createdTime = new Number(notebook.createdTime);
    this.notebookSectionArray = [];
    for(ni=0; ni < notebook.notebookSectionList.length; ni++) {
        var section = notebook.notebookSectionList[ni];
        var newSection = new NotebookSection(section);
        this.notebookSectionArray.push(newSection);
    }
}
Notebook.prototype.getSection = function (sectionID) {
    for (i = 0; i < this.notebookSectionArray.length; i++) {
        if (sectionID == this.notebookSectionArray[i].id) {
            return this.notebookSectionArray[i];
        }
    }
    return null;
}
Notebook.prototype.addSection = function (section) {
    if (!this.getSection(section.id)) {
        var newSection = new NotebookSection(section);
        this.notebookSectionArray.push(newSection);
    }
}
Notebook.prototype.updateSection = function (section) {
    for (i = 0; i < this.notebookSectionArray.length; i++) {
        if (section.id == this.notebookSectionArray[i].id) {
            this.notebookSectionArray[i].sectionName = section.sectionName;
            return;
        }
    }
}
Notebook.prototype.deleteSection = function (sectionID) {
    for (i = 0; i < this.notebookSectionArray.length; i++) {
        if (sectionID == this.notebookSectionArray[i].id) {
            this.notebookSectionArray.splice(i, 1);
            return;
        }
    }
}
Notebook.prototype.sortSections = function(isRecursive) {
    if (isRecursive) {
        for (i = 0; i < this.notebookSectionArray.length; i++) {
            this.notebookSectionArray[i].sortPages(true);
        }
    }
    this.notebookSectionArray.sort(comparator_sort);
}

/*************************************************************
 * USER DATA MODEL
 ************************************************************/
var User = function (user) {
    this.id = user.id;
    this.firstName = user.firstName;
    this.lastName = user.lastName;
    this.userName = user.userName;
    this.createdDate = user.createdDate;
    this.activatedDate = user.activatedDate;
    this.notebookArray = [];
    this.taskgroupArray = [];
    this.labelArray = [];
    this.emailAccountArray = user.userEmailList;
}
User.prototype.getNotebooks = function () {
    return this.notebookArray;
};
User.prototype.getTaskGroups = function () {
    return this.taskgroupArray;
};
User.prototype.getEmailAccounts = function () {
    return this.emailAccountArray;
};
User.prototype.addNotebook = function (notebook) {
    if (!this.getNotebook(notebook.id)) {
        var newNotebook = new Notebook(notebook);
        this.notebookArray.push(newNotebook);
    }
}
User.prototype.updateNotebook = function (notebook) {
    for (i = 0; i < this.notebookArray.length; i++) {
        if (notebook.id == this.notebookArray[i].id) {
            this.notebookArray[i].notebookName = notebook.notebookName;
            return;
        }
    }
}
User.prototype.getNotebook = function (notebookID) {
    for (i = 0; i < this.notebookArray.length; i++) {
        if (notebookID == this.notebookArray[i].id) {
            return this.notebookArray[i];
        }
    }
    return null;
}
User.prototype.addTaskGroup = function (taskGroup) {
    if (!this.getTaskGroup(taskGroup.id)) {
        var newTaskGroup = new TaskGroup(taskGroup);
        this.taskgroupArray.push(newTaskGroup);
    }
}
User.prototype.getTaskGroup = function (taskGroupID) {
    for (i = 0; i < this.taskgroupArray.length; i++) {
        if (taskGroupID == this.taskgroupArray[i].id) {
            return this.taskgroupArray[i];
        }
    }
    return null;
}
User.prototype.updateTaskGroup = function (taskGroup) {
    for (i = 0; i < this.taskgroupArray.length; i++) {
        if (taskGroup.id == this.taskgroupArray[i].id) {
            this.taskgroupArray[i].taskGroupName = taskGroup.taskGroupName;
            return;
        }
    }
}
User.prototype.addLabel = function(label) {
    if (label) {
        var newLabel = new Label(label);
        this.labelArray.push(newLabel);
    }
}
User.prototype.getLabelByID = function(labelID) {
    if (labelID) {
        for (i=0; i<this.labelArray.length; i++)
            if(this.labelArray[i].id == labelID)
                return this.labelArray[i];
    }
}
User.prototype.getLabelByName = function(labelName) {
    if (labelName) {
        for (i=0; i<this.labelArray.length; i++)
            if(this.labelArray[i].labelName == labelName)
                return this.labelArray[i];
    }
}
User.prototype.getLabels = function() {
    var labels = [];
    for(i=0; i<this.labelArray.length; i++) {
        labels.push(this.labelArray[i].labelName);
    }
    return labels;
}

/*
User.prototype.sortTaskGroups = function (isRecursive) {
    if (isRecursive) {
        for (i = 0; i < this.taskgroupArray.length; i++) {
            this.taskgroupArray[i].sortSections(true);
        }
    }
    this.taskgroupArray.sort(comparator_sort);
};
*/
/*User.prototype.deleteTaskGroup = function (taskGroupID) {
 for (i = 0; i < this.taskgroupArray.length; i++) {
 if (taskGroupID == this.taskgroupArray[i].id) {
 this.taskgroupArray.splice(i, 1);
 return;
 }
 }
 }*/

/*User.prototype.deleteNotebook = function (notebookID) {
 for (i = 0; i < this.notebookArray.length; i++) {
 if (notebookID == this.notebookArray[i].id) {
 this.notebookArray.splice(i, 1);
 return;
 }
 }
 };*/
/*User.prototype.sortNotebooks = function (isRecursive) {
 if (isRecursive) {
 for (i = 0; i < this.notebookArray.length; i++) {
 this.notebookArray[i].sortSections(true);
 }
 }
 this.notebookArray.sort(comparator_sort);
 };*/
