/**
 * Created by Digitay Development Team: Siddhartha Moitra, Nitesh Singh on 15.07.16.
 */

function search_entities_labels(e) {
    e = e || window.event;

    $('#tagged-notes').empty();
    $('#tagged-tasks').empty();
    $('#tagged-emails').empty();

    var spinner = "<div class='text-center' style='height: 80%;'><h3><i class='fa fa-cog fa-spin'></i> Requesting API for data... </h3></div>";

    $('#tagged-notes').append(spinner);
    $('#tagged-tasks').append(spinner);
    $('#tagged-emails').append(spinner);
    
    var labelIDs = [];
    $('#check-list-box input:checkbox').each(function () {
        if(this.checked) {
            labelIDs.push(USR_PROFILE.getLabelByName($(this).val().trim()).id);
        }
    });

    get_all_entities_by_labels(labelIDs, true, function(entities){
        var templateData = new Object();
        templateData.noteList = entities.noteList;
        html_template = get_inner_HTML("templates/common/list-note.html");
        template = _.template(html_template);
        $('#tagged-notes').empty();
        $('#tagged-notes').append(template(templateData));

        templateData = new Object();
        templateData.taskList = entities.taskList;
        html_template = get_inner_HTML("templates/common/list-task.html");
        template = _.template(html_template);
        $('#tagged-tasks').empty();
        $('#tagged-tasks').append(template(templateData));

        templateData = new Object();
        templateData.conversationList = entities.emailConversationList;
        html_template = get_inner_HTML("templates/common/list-conversation.html");
        template = _.template(html_template);
        $('#tagged-emails').empty();
        $('#tagged-emails').append(template(templateData));

        // Add event handlers to show tagged Emails
        $('.conversation-message-header').on('click', function(e) {
            e.stopPropagation();
            $(e.target).closest('div.conversation-message-header').next().toggleClass('show hide');
            $(e.target).closest('div.conversation-message-header').toggleClass('selected');
        });
    });
}
