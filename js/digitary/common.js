/**
 * Created by Digitay Development Team: Siddhartha Moitra, Nitesh Singh on 17.04.16.
 */

const APP_ID = "353b302c44574f565045687e534e7d6a";
//const APP_ID = "286924697e615a672a646a493545646c";

//const API_ROOT_URL = "https://localhost:8443/";
const API_ROOT_URL = "https://digitary.cs.upb.de:8443/";
const APIS = new Map();
APIS.set("GET_USER_AVAILABLE", API_ROOT_URL + "api/v1/users/{username}/available/");
APIS.set("REGISTER_USER", API_ROOT_URL + "api/v1/users/");
APIS.set("GET_USER_LOGIN", API_ROOT_URL + "api/v1/token/");
APIS.set("GET_BEARER_AUTHENTICATION", API_ROOT_URL + "api/v1/authenticate/");
APIS.set("GET_USER_PROFILE", API_ROOT_URL + "api/v1/auth/users/{userID}/");
APIS.set("DELETE_USER_PROFILE", API_ROOT_URL + "api/v1/auth/users/{userID}/");
APIS.set("GET_USER_LOGOUT", API_ROOT_URL + "api/v1/auth/token/");
APIS.set("GET_NOTEBOOK_HIERARCHY", API_ROOT_URL + "api/v1/auth/notebooks/{notebookID}/");
APIS.set("UPDATE_NOTEBOOK", API_ROOT_URL + "api/v1/auth/notebooks/{notebookID}/");
APIS.set("CREATE_NOTEBOOK", API_ROOT_URL + "api/v1/auth/notebooks/");
APIS.set("GET_NOTEBOOK_PAGE_NOTES", API_ROOT_URL + "api/v1/auth/notebooks/{notebookID}/sections/{xpath}/pages/{pageID}/notes/");
APIS.set("DELETE_NOTEBOOK_PAGE_NOTE", API_ROOT_URL + "api/v1/auth/notebooks/{notebookID}/sections/{xpath}/pages/{pageID}/notes/{noteID}/");
APIS.set("UPDATE_NOTEBOOK_PAGE_NOTE", API_ROOT_URL + "api/v1/auth/notebooks/{notebookID}/sections/{xpath}/pages/{pageID}/notes/{noteID}/");
APIS.set("CREATE_NOTEBOOK_PAGE_NOTE", API_ROOT_URL + "api/v1/auth/notebooks/{notebookID}/sections/{xpath}/pages/{pageID}/notes/");
APIS.set("CREATE_SECTION_PAGE", API_ROOT_URL + "api/v1/auth/notebooks/{notebookID}/sections/{xpath}/pages/");
APIS.set("CREATE_SECTION", API_ROOT_URL + "api/v1/auth/notebooks/{notebookID}/sections/");
APIS.set("UPDATE_SECTION", API_ROOT_URL + "api/v1/auth/notebooks/{notebookID}/sections/{xpath}/");
APIS.set("DELETE_SECTION", API_ROOT_URL + "api/v1/auth/notebooks/{notebookID}/sections/{xpath}/");
APIS.set("UPDATE_PAGE", API_ROOT_URL + "api/v1/auth/notebooks/{notebookID}/sections/{xpath}/pages/{pageID}/");
APIS.set("DELETE_PAGE", API_ROOT_URL + "api/v1/auth/notebooks/{notebookID}/sections/{xpath}/pages/{pageID}/");
APIS.set("GET_TASK_GROUPS", API_ROOT_URL + "api/v1/auth/taskgroups/{taskGroupID}/");
APIS.set("UPDATE_TASK_GROUPS", API_ROOT_URL + "api/v1/auth/taskgroups/{taskGroupID}/");
APIS.set("DELETE_TASK_GROUPS", API_ROOT_URL + "api/v1/auth/taskgroups/{taskGroupID}/");
APIS.set("CREATE_TASK_GROUPS", API_ROOT_URL + "api/v1/auth/taskgroups/");
APIS.set("GET_TASK", API_ROOT_URL + "api/v1/auth/taskgroups/{taskGroupID}/tasks/");
APIS.set("UPDATE_TASK", API_ROOT_URL + "api/v1/auth/tasks/{taskID}/");
APIS.set("CREATE_TASK", API_ROOT_URL + "api/v1/auth/taskgroups/{taskGroupID}/tasks/");
APIS.set("DELETE_TASK", API_ROOT_URL + "api/v1/auth/tasks/{taskID}/");
APIS.set("GET_LABELS", API_ROOT_URL + "api/v1/auth/labels/");
APIS.set("GET_ENTITIES_BY_LABEL", API_ROOT_URL + "api/v1/auth/labels/entities/");
APIS.set("CREATE_LABELS", API_ROOT_URL + "api/v1/auth/labels/");
APIS.set("GET_ASSOCIATED_ENTITIES", API_ROOT_URL + "api/v1/auth/associations/{entityID}/list/");
APIS.set("CREATE_ASSOCIATIONS", API_ROOT_URL + "api/v1/auth/associations/");
APIS.set("DELETE_ASSOCIATIONS", API_ROOT_URL + "api/v1/auth/associations/");
APIS.set("GET_CONVERSATIONS", API_ROOT_URL + "api/v1/auth/emails/accounts/{accountID}/conversations/");
APIS.set("GET_SENT_CONVERSATIONS", API_ROOT_URL + "api/v1/auth/emails/accounts/{accountID}/conversations/");
APIS.set("ARCHIVE_CONVERSATIONS", API_ROOT_URL + "api/v1/auth/emails/accounts/{accountID}/conversations/{conversationID}/archive/");
APIS.set("DELETE_CONVERSATIONS", API_ROOT_URL + "api/v1/auth/emails/accounts/{accountID}/conversations/{conversationID}/");
APIS.set("UPDATE_CONVERSATION_STATUS", API_ROOT_URL + "api/v1/auth/emails/accounts/{accountID}/conversations/{conversationID}/read/");
APIS.set("SEND_EMAIL", API_ROOT_URL + "api/v1/auth/emails/accounts/{accountID}/messages/");
APIS.set("GET_NOTE", API_ROOT_URL + "api/v1/auth/notebooks/notes/{noteID}/");
APIS.set("SEARCH", API_ROOT_URL + "api/v1/auth/search/");
APIS.set("SEARCH_EMAIL", API_ROOT_URL + "api/v1/auth/emails/accounts/{accountID}/search/");
APIS.set("EMAIL_REQ_AUTH", API_ROOT_URL + "api/v1/auth/gmail/oauth/");
APIS.set("EMAIL_TOKEN_AUTH", API_ROOT_URL + "api/gmail/token/");

const COOKIE_SID = "SID";
const COOKIE_UID = "UID";

//const KEY_USR_PRFL = "usr_prfl";
//const APP_ROOT = "https://localhost:8443/";
const APP_ROOT = "https://digitary.cs.upb.de:8443/";

const ENTITY_NOTE = "O_NOTE";
const ENTITY_TASK = "O_TASK";
const ENTITY_EMAIL = "O_EMAIL_MESSAGE";
const ENTITY_NB_PAGE = "O_NOTEBOOK_SECTION_PAGE";
const ENTITY_NB_SECTION = "O_NOTEBOOK_SECTION";
//const ENTITY_ASSOCIATION = "O_ASSOCIATION";
const TASK_TO_NOTE = "2";
//const EMAIL_TO_NOTE = "3";
//const EMAIL_TO_TASK = "4";

const EMAIL_ALL_INBOX = "ALL_INBOX";
const EMAIL_UNREAD_INBOX = "UNREAD_INBOX";
const EMAIL_ALL_SENT = "ALL_SENT";

const MILLISECONDS_DAY = 86400000;

/*
 * Constants for Tasks
 */
const TASK_OPEN = 1;
const TASK_COMPLETE = 2;

const TASK_PRIORITY_HIGH = 1;
const TASK_PRIORITY_MEDIUM = 2;
const TASK_PRIORITY_LOW = 3;
const TASK_PRIORITY_TRIVIAL = 4;

/*
 * Constants for Notebook
 */
const NAME_NEW_SECTION = "Untitled Section";
const NAME_NEW_PAGE = "Untitled Page";

function is_touch_device() {
    try {
        document.createEvent("TouchEvent");
        return true;
    } catch (e) {
        return false;
    }
}

const IS_TOUCH_ENABLED = is_touch_device();

var USR_PROFILE = {};

function associate_tinymce_editor_with_id(id) {
    // Associate editor with the notes
    tinymce.init({
        selector: '#' + id,
        inline: true,
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist",
        menubar: false,
        fontsize_formats: "14pt"
    });
}
function associate_tinymce_editor_with_class(cla) {
    // Associate editor with the notes
    tinymce.init({
        selector: '.' + cla,
        inline: true,
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist",
        menubar: false,
        fontsize_formats: "14pt"
    });
}
function associate_tinymce_editor_notinline_with_id(id) {
    // Associate editor with the notes
    tinymce.init({
        selector: '#' + id,
        inline: false,
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist",
        menubar: false,
        fontsize_formats: "14pt"
    });
}
function associate_tinymce_editor_noteinline_with_class(cla) {
    // Associate editor with the notes
    tinymce.init({
        selector: '.' + cla,
        inline: false,
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist",
        menubar: false,
        fontsize_formats: "14pt"
    });
}

function load_base_modal(modalTitle) {
    var html_template = get_inner_HTML("templates/modals/base-modal.html");
    $('#modal-section').empty();
    $('#modal-section').append(html_template);
    // Append the title now
    $('#base-modal').find('h5.modal-title').append(modalTitle);

    // Code to flush modal from DOM completely
    $("#base-modal").on('hidden.bs.modal', function () {
        $(this).remove();
        $('.modal-backdrop').remove();
    });
    // Show this modal now
    $('#base-modal').modal({show: true});
}

function show_data_on_base_modal(htmlData) {
    var modal_body = $('#base-modal').find('div.modal-body');
    // Empty body
    $(modal_body).empty();
    $(modal_body).removeClass('text-center');
    // Add content
    $(modal_body).append(htmlData);
}
function load_base_modal_sm(modalTitle) {
    var html_template = get_inner_HTML("templates/modals/base-modal-sm.html");
    $('#modal-section').empty();
    $('#modal-section').append(html_template);
    // Append the title now
    $('#base-modal-sm').find('h5.modal-title').append(modalTitle);

    // Code to flush modal from DOM completely
    $("#base-modal-sm").on('hidden.bs.modal', function () {
        $(this).remove();
        $('.modal-backdrop').remove();
    });
    // Show this modal now
    $('#base-modal-sm').modal({show: true});
}

function show_data_on_base_modal_sm(htmlData) {
    var modal_body = $('#base-modal-sm').find('div.modal-body');
    // Empty body
    $(modal_body).empty();
    $(modal_body).removeClass('text-center');
    // Add content
    $(modal_body).append(htmlData);
}
function show_message_on_base_modal(messageHtml) {
    var modal_footer = $('#base-modal').find('div.modal-footer');
    // Empty footer
    $(modal_footer).empty();
    // Add content
    if (messageHtml instanceof DigitaryWebError) {
        $(modal_footer).append('<b>Ooppss...</b> Something went bad. Could not complete the request.');
    } else {
        $(modal_footer).append(messageHtml);
    }
}

function load_tagged_entities_by_label_name(e) {
    e = e || window.event;
    var labelName = $(e.target).text().trim();
    if(labelName) {
        var label = USR_PROFILE.getLabelByName(labelName);

        load_base_modal("Entities tagged by label: <b>" + labelName + "</b>");
        // once the modal is loaded, I will get data.

        get_all_entities_by_label(label.id, true, function(entities){
            var templateData = new Object();
            var html_template = get_inner_HTML("templates/modals/tagged-entities-result.html");
            var template = _.template(html_template);
            show_data_on_base_modal(template(templateData));

            templateData = new Object();
            templateData.noteList = entities.noteList;
            html_template = get_inner_HTML("templates/common/list-note.html");
            template = _.template(html_template);
            $('#labelled-notes').append(template(templateData));

            templateData = new Object();
            templateData.taskList = entities.taskList;
            html_template = get_inner_HTML("templates/common/list-task.html");
            template = _.template(html_template);
            $('#labelled-tasks').append(template(templateData));

            templateData = new Object();
            templateData.conversationList = entities.emailConversationList;
            html_template = get_inner_HTML("templates/common/list-conversation.html");
            template = _.template(html_template);
            $('#labelled-emails').append(template(templateData));

            // Add event handlers to show tagged Emails
            $('.conversation-message-header').on('click', function(e) {
                e.stopPropagation();
                $(e.target).closest('div.conversation-message-header').next().toggleClass('show hide');
                $(e.target).closest('div.conversation-message-header').toggleClass('selected');
            });

        });
    }
}
function show_success_message(msg){
    $.notify({
        title: "<strong>Success:</strong> ",
        message: msg
    });
}
function show_error_message(msg, error){
    $.notify({
        title: "<strong>Error:</strong> ",
        message: msg + '<br><b>Details:</b>' + error.error.causeMessage
    },
        {
            type: 'danger'
        });
}
//convert html to martdown
function convert_html_to_markdown(htmlData, callback){
    var und = new upndown();
    und.convert(htmlData, function(err, markdown) {
        if(err) {
            console.err(err);
        }
        else {
            callback(markdown);
        }
    });
}
function DigitaryWebError(message, jqXHR) {
    this.message = message;
    this.error = $.parseJSON(jqXHR.responseText);
}

function handle_error(error) {
    if (error.apiError.apiError == 'AUTH_102') {
        // Remove cookies
        var today = new Date();
        var cookieExpire = new Date();
        cookieExpire.setDate(today.getDate() - 3650);
        cookies.set(COOKIE_SID, user_access.accessToken, {path: '/', expires: cookieExpire});
        //cookies.set(COOKIE_UID, username, {path: '/', expires: cookieExpire});
        cookies.set(COOKIE_UID, userID, {path: '/', expires: cookieExpire});


    }
}

/**
 * This function replaces the URLs in API with the URL resource string from the map.
 *
 * @param url_key the internal key of the URL
 * @param url_resource_map key value pairs of URL resource name and
 *              it's corresponding value that must be substituted
 */
function preprocess_html_escape(part) {
    var new_part = part;

    new_part = new_part.replace(/#/g, '%23');
    new_part = new_part.replace(/:/g, '%3A');
    new_part = new_part.replace(/\./g, '%2E');

    return new_part;
}
function normalize_html_escape(part) {
    var new_part = part;

    new_part = new_part.replace(/%23/g, '#');
    new_part = new_part.replace(/%3A/g, ':');
    new_part = new_part.replace(/%2E/g, '.');

    return new_part;
}

function get_start_time_week() {
    // returns this week's start
    return moment.utc().startOf('week').valueOf();
}

function get_end_time_week() {
    // returns this week's end
    return moment.utc().endOf('week').valueOf();
}

function get_start_time_today() {
    // returns today's start
    return moment.utc().hour(0).minute(0).second(0).millisecond(0).valueOf();
}

function get_end_time_today() {
    // returns today's end
    return moment.utc().hour(23).minute(59).second(59).millisecond(999).valueOf();
}

function get_simple_text(markdownText) {
    return markdownText.replace(/[^a-zA-Z0-9 ]/g, "");
}

function get_snippet(simpleText) {
    if (simpleText && simpleText.length > 20) {
        return simpleText.substring(0, 35) + "...";
    } else {
        return simpleText + "...";
    }
}
//To display noteBody in search
function get_noteSnippet(simpleText) {
    if (simpleText && simpleText.length > 20) {
        return simpleText.substring(0, 90) + "...";
    } else {
        return simpleText + "...";
    }
}

function get_final_computable_url(url_key, url_resource_map) {
    var url = APIS.get(url_key);
    if (url_resource_map && url_resource_map.size > 0) {
        url_resource_map.forEach(function (value, key) {
            value = preprocess_html_escape(value);
            url = url.split(key).join(value);
        });
    }
    return url;
}

function get_html_id_from_internal_id(internalID) {
    var html_id = '';
    var regex = /#\d+:\d+/;
    if (regex.test(internalID)) {
        html_id = internalID.replace(/#/, "hash");
        html_id = html_id.replace(/:/, "c");
    }
    return html_id;
}

function get_internal_id_from_html_id(htmlID) {
    var internal_id = '';
    var regex = /hash\d+c\d+/;
    if (regex.test(htmlID)) {
        internal_id = htmlID.replace(/hash/, "#");
        internal_id = internal_id.replace(/c/, ":");
    }
    return internal_id;
}

function get_js_date(date) {
    var page_createdDate = Number(date);
    var date_options = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        /*hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'*/
    };
    return new Date(page_createdDate).toLocaleDateString('de-DE', date_options);
}

function get_js_date_short(date) {
    var mdate = moment(date).format("MM/DD/YYYY");
    return mdate;
}
function get_long_date(dateString) {
    var tokens = dateString.split('/');
    var mdate = moment.utc([parseInt(tokens[2]), parseInt(tokens[0]), parseInt(tokens[1]), 23, 59, 59, 999]);
    return mdate.valueOf();
}

function get_inner_HTML(htmlPath) {
    var dom = "";
    $.ajax({
        url: htmlPath,
        async: false,
        context: document.body,
        success: function (responseText) {
            dom = responseText;
        }
    });

    return dom;
}

function load_inner_HTML(htmlPath) {
    var dom = get_inner_HTML(htmlPath);
    if (dom == '')
        throw new DigitaryWebError("Cannot load inner html", {});

    var html = $(dom);
    html.filter('script').each(function () {
        $.globalEval(this.text || this.textContent || this.innerHTML || '');
    });
    return html;
}

function get_task_priority_name(task_priority) {
    if (task_priority == TASK_PRIORITY_HIGH)
        return "HIGH";
    else if (task_priority == TASK_PRIORITY_MEDIUM)
        return "MEDIUM";
    else if (task_priority == TASK_PRIORITY_LOW)
        return "LOW";
    else if (task_priority == TASK_PRIORITY_TRIVIAL)
        return "TRIVIAL";
}

$(document).ready(function () {
    
});
