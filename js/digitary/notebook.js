/**
 * Created by Digitay Development Team: Siddhartha Moitra, Nitesh Singh on 19.04.16.
 */

function toggle_notebook_content() {
    $(".notebook-wrapper").toggleClass("toggled");
}

function change_page_content_on_section_click(e) {
    e = e || window.event;
    // This is a hack because the page content is not a complete part of tabbed page list
    var active_section_html_id = $(e.target).parent().attr('id');
    // Get the active page id in this section
    var active_page_html_id = $('#section_content_'+active_section_html_id).find('li.active').attr('id');
    var active_page = $('div.notebook-content-wrapper .tab-content').children().removeClass('active');
    $('#page_content_' + active_page_html_id).addClass('active');
}

function load_notes_ui(pageID, notes) {
    var page_html_id = 'page_content_' + get_html_id_from_internal_id(pageID);

    var note_labels = [];
    $.each(notes, function(i, note) {
        $.each(note.labelIDList, function (index, label){
            var label =  USR_PROFILE.getLabelByID(label);
            note_labels.push(label);
        });
        var templateData = new Object();
        templateData.note = note;
        templateData.labels = note_labels;
        templateData.isNew = false;
        var html_template = get_inner_HTML("templates/notebook/notebook-note.html");
        var template = _.template(html_template);
        $('#' + page_html_id + ' .page-content').append(template(templateData));
        associate_tinymce_editor_with_id("note_body_" + get_html_id_from_internal_id(note.id));
    });
}

function load_notebook_content_ui(notebook) {
    var notebook_html_id = get_html_id_from_internal_id(notebook.id);
    $.each(notebook.notebookSectionArray, function(i, section){
        $.each(section.notebookSectionPageArray, function(j, page){
            // Get page notes
            var map = new Map();
            map.set("{notebookID}", notebook.id);
            map.set("{xpath}", section.id);
            map.set("{pageID}", page.id);

            get_notebook_section_page_notes_call(map, true, function (notes) {
                // For each page we load the template
                // TODO Create note class and use
                // Now we load the underscore template
                var templateData = new Object();
                templateData.page = page;
                /*templateData.notes = notes;*/
                // Load the html template file
                var html_template = get_inner_HTML("templates/notebook/notes-content.html");
                var template = _.template(html_template);
                $('#' + notebook_html_id + ' .notebook-wrapper .notebook-content-wrapper').children('.tab-content').append(template(templateData));
                // Once this partial HTML is in DOM, we update notes
                load_notes_ui(page.id, notes);
                // Activate on the page content whose page and section is active.
                var activeSectionHtmlID = $('div.section-tabs-left ul').find('li.active').attr('id');
                var activeSectionID = get_internal_id_from_html_id(activeSectionHtmlID);
                if(activeSectionID == section.id) {
                    // Now find active page in this section
                    var contentSectionID = "section_content_" + activeSectionHtmlID;
                    var pageHtmlID = $('#' + contentSectionID + " div.page-tabs-right ul").find('li.active').attr('id');
                    var makeActiveID = "page_content_" + pageHtmlID;
                    if(!($('#' + makeActiveID).hasClass('active'))) {
                        $('#' + makeActiveID).addClass('active');
                    }
                }
            });
        });
    });
}

function load_notebook_ui(notebook) {
    // Now we load the underscore template
    var templateData = new Object();
    templateData.notebook = notebook;
    // Load the html template file
    var html_template = get_inner_HTML("templates/notebook/notebook-base.html");
    var template = _.template(html_template);
    $('#notebook').empty();
    $('#notebook').append(template(templateData));
    $("#select-notebook").val(notebook.notebookName);
    $("#select-notebookID").val(notebook.id);
    load_notebook_content_ui(notebook);
}

function add_event_handler_left_view() {
    // Hide control div when in focus
    $('.note').on('focusin', function () {
        $(this).prev().hide();
    })
    $('.note').on('blur', function () {
        $(this).prev().show();
    })
    // Associate editors with the notes
    tinymce.init({
        selector: '.note',
        inline: true,
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist",
        menubar: false,
        fontsize_formats: "14pt"
    });

    $('.add-note').on('click', function () {
        $(this).parent().parent().append();
    });
}

//Delete Note code start
$('body').on('click', 'a.delete-note', function () {
    var $this = $(this);
    if ($(this).closest('.new-note').length > 0) {
        $(this).closest('div.new-note').remove();
    }
    else {
        var html_note_ID = $(this).closest('ul').parent().parent().parent().parent().attr('id');
        var noteID = get_internal_id_from_html_id(html_note_ID);
        var noteBody = $(this).closest('ul').parent().parent().children('div').children('p').html();
        var timeUpdated = moment().utc().valueOf();
        var html_notebookID = $(this).closest('ul').parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().attr('id');
        var noteBookID = get_internal_id_from_html_id(html_notebookID);
        var html_sectionID = $(this).closest('ul').parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().children().children().children().children('div').find('ul').children('li.active').attr('id');
        var sectionID = get_internal_id_from_html_id(html_sectionID);
        var html_pageID = $(this).closest('ul').parent().parent().parent().parent().parent().parent().parent().parent().parent().prev().children('div:nth-child(2)').children('div').children('div.active').children('div').find('ul').children('li.active').attr('id');
        var pageID = get_internal_id_from_html_id(html_pageID);

        var map = new Map();
        map.set("{notebookID}", noteBookID);
        map.set("{xpath}", sectionID);
        map.set("{pageID}", pageID);
        map.set("{noteID}", noteID);
        delete_note_call(map, true, function (note) {
            if (note instanceof DigitaryWebError) {
                //console.log("Cannot delete note: " + note.error.causeMessage);
                show_error_message("Could not delete note.", note);
            }
            else {
                //console.log('Note Successfully deleted');
                show_success_message("Successfully deleted note.");
            }

        });
    }
    $(this).closest('div.panel-default').remove();
});
//Delete Note code ends
//Add Note code starts
function add_new_note(e) {
    e = e || window.event;
    add_note($(e.target).parent());
}

function add_note(container, note) {
    var NOTE_CONTENT_01 = "";
    if(note) {
        var templateData = new Object();
        templateData.note = note;
        var note_labels = [];
        $.each(note.labelIDList, function (index, label){
            var label =  USR_PROFILE.getLabelByID(label);
            note_labels.push(label);
        });
        templateData.labels = note_labels;
        templateData.isNew = false;
        var html_template = get_inner_HTML("templates/notebook/notebook-note.html");
        var template = _.template(html_template);
        $(container).append(template(templateData));
        associate_tinymce_editor_with_id('note_body_' + get_html_id_from_internal_id(note.id));
    } else {
        var templateData = new Object();
        templateData.note = {};
        templateData.labels = [];
        templateData.isNew = true;
        var html_template = get_inner_HTML("templates/notebook/notebook-note.html");
        var template = _.template(html_template);
        $(container).append(template(templateData));
        associate_tinymce_editor_with_class('note');
    }
}
$('body').on("click", "a.update-note", function () {
    //create Note
    var $this = $(this);
   if ($(this).closest('.new-note').length > 0) {
        //var noteBody = $(this).closest('ul').parent().parent().children('div').children('p').html();
        var noteBody = tinyMCE.activeEditor.getContent();
        var html_sectionID = $(this).closest('ul').parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().children().children().children().children('div').find('ul').children('li.active').attr('id');
        var sectionID = get_internal_id_from_html_id(html_sectionID);
        var html_page_ID = $(this).closest('ul').parent().parent().parent().parent().parent().parent().parent().parent().parent().prev().children('div:nth-child(2)').children('div').children('div.active').children('div').find('ul').children('li.active').attr('id');
        var pageID = get_internal_id_from_html_id(html_page_ID);
        var html_notebookID = $(this).closest('ul').parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().attr('id');
        var notebookID = get_internal_id_from_html_id(html_notebookID);
        var createdTime = moment().utc().valueOf();
        var timeUpdated = moment().utc().valueOf();

       convert_html_to_markdown(noteBody, function (markdown) {
           var currentNote = new Object();
           currentNote.noteBody = markdown;
           //currentNote.createdTime = createdTime;
           currentNote.timeUpdated = timeUpdated;
           currentNote.createdBy = USR_PROFILE.userName;
           var map = new Map();
           map.set("{notebookID}", notebookID);
           map.set("{xpath}", sectionID);
           map.set("{pageID}", pageID);
           create_note_call(map, currentNote, true, function (note) {
               if (note instanceof DigitaryWebError) {
                   //console.log("Cannot Create Note: " + note.error.causeMessage);
                   show_error_message("Could not create note.",note);
               } else {
                   $($this).closest('ul').parent().parent().parent().parent().removeClass('new-note');
                   var note_html_id = get_html_id_from_internal_id(note.id);
                   $($this).closest('div.note-content').children('div').attr('id', 'note_body_'+note_html_id);
                   //$($this).closest('div.note-content').children('div').append("<div class='d-note'></div>");
                   $($this).closest('ul').parent().parent().parent().parent().attr('id', note_html_id);
                   $($this).closest('ul').append("<li><a href='#' data-toggle='tooltip' class='associative-editor' title='Open in Associative Editor' onclick='javascript: load_associative_editor_note(event);'>" +
                       "<span class='label label-info label-as-badge'><i class='fa fa-life-saver'></i> Associative Editor </span></a></li>" +
                       "<li><a href='#' data-toggle='tooltip' class='create-task-forNote' title='Create task for this note' onclick='javascript: create_task_from_note(event);'>" +
                       "<span class='label label-primary label-as-badge'><i class='fa fa-tasks'></i> Create Task </span></a></li>" +
                       "<li class='show-label'></li>");
                   show_success_message("Successfully created note.");
               }
           });
       });


   } else { //Update Note
       var html_noteID = $(this).closest('div.panel-default').attr('id');
       var noteID = get_internal_id_from_html_id(html_noteID);
       //var note_Body = $(this).closest('div.note-content').children('div').html();
       //var note_Body = tinyMCE.get('note_body_'+html_noteID).getContent();
       var note_Body = tinyMCE.activeEditor.getContent({format: 'raw'});
       var timeUpdated = moment().utc().valueOf();
       var html_notebook_ID = $(this).closest('div.notebook-wrapper').parent().attr('id');
       var noteBookID = get_internal_id_from_html_id(html_notebook_ID);
       var html_section_ID = $(this).closest('div.notebook-wrapper').children('div.notebook-sidebar-wrapper').children('div.notebook-sidebar-left').children('div.section-tabs-left').find('ul').children('li.active').attr('id');
       var section_ID = get_internal_id_from_html_id(html_section_ID);
       var html_pageID = $(this).closest('div.notebook-wrapper').children('div.notebook-sidebar-wrapper').children('div.notebook-sidebar-right').children('div.tab-content').children('div.active').children('div').find('ul').children('li.active').attr('id');
       var page_ID = get_internal_id_from_html_id(html_pageID);
       convert_html_to_markdown(note_Body, function (markdown) {
           var newNote = new Object();
           newNote.id = noteID;
           //newNote.createdTime = note.createdTime;
           newNote.noteBody = markdown;
           newNote.timeUpdated = timeUpdated;
           newNote.createdBy = USR_PROFILE.userName;
           var map = new Map();
           map.set("{notebookID}", noteBookID);
           map.set("{xpath}", section_ID);
           map.set("{pageID}", page_ID);
           map.set("{noteID}", noteID);

           update_note_call(map, newNote, true, function (note) {
               if (note instanceof DigitaryWebError) {
                   //console.log("Cannot update note: " + note.error.causeMessage);
                   show_error_message("Could not update note.", note);
               } else {
                   show_success_message("Successfully updated note.");
               }
           });
       });
   }
});
function add_page(e) {
    var notebookID = get_internal_id_from_html_id($('div.notebook-wrapper').parent().attr('id'));
    var sectionID = get_internal_id_from_html_id($('div.section-tabs-left ul').find('li.active').attr('id'));
    // Now lets create a page
    var newPage = new Object();
    newPage.pageName = NAME_NEW_PAGE;

    var map = new Map();
    map.set("{notebookID}", notebookID);
    map.set("{xpath}", sectionID);

    create_page_call(map, newPage, true, function (page) {
        if (page instanceof DigitaryWebError) {
            //console.log("Cannot create page: " + page.error.causeMessage);
            show_error_message("Could not create page.", page);
        } else {
            var pageHtmlID = get_html_id_from_internal_id(page.id);
            var new_page_html = "<li class='active' id='" + pageHtmlID + "'>";
            //new_page_html = new_page_html + "<a href='#page_content_'" + pageHtmlID + "' data-toggle='tab'>" + page.pageName + "</a></li>";
            new_page_html = new_page_html + "<a href='#page_content_" + pageHtmlID + "' data-toggle='tab' ondblclick='javascript: edit_section_or_page(ENTITY_NB_PAGE);'>" + page.pageName + "</a></li>";
            var section_html_ID = get_html_id_from_internal_id(sectionID);
            $('#section_content_' + section_html_ID).find('ul').children().removeClass('active');
            $('#section_content_' + section_html_ID).find('ul').append(new_page_html);

            var templateData = new Object();
            templateData.page = page;
            // Load the html template file
            var html_template = get_inner_HTML("templates/notebook/notes-content.html");
            var template = _.template(html_template);
            $('div.notebook-content-wrapper .tab-content').append(template(templateData));
            $('div.notebook-content-wrapper .tab-content').children().removeClass('active');
            var pageHtmlID = get_html_id_from_internal_id(page.id);
            $('#page_content_' + pageHtmlID).addClass('active');
            show_success_message("Successfully created page.");
        }
    });
}

//Add New Section
function add_section(e) {
    e = e || window.event;
    var notebookID = get_internal_id_from_html_id($(e.target).closest('div.notebook-wrapper').parent().attr('id'));
    var map = new Map();
    map.set("{notebookID}", notebookID);

    var newSection = new Object();
    newSection.sectionName = NAME_NEW_SECTION;

    create_section_call(map, newSection, true, function (section) {
        if (section instanceof DigitaryWebError) {
            //console.log("Cannot create page: " + section.error.causeMessage);
            show_error_message("Could not create section.",section);
        } else {
            var sectionHtmlID = get_html_id_from_internal_id(section.id);
            var new_section_html = "<li class='active' id='" + sectionHtmlID + "'>";
            new_section_html = new_section_html + "<a href='#section_content_"+sectionHtmlID+"' data-toggle='tab'ondblclick='javascript: edit_section_or_page(ENTITY_NB_SECTION);'>" + section.sectionName + "</a></li>";
            // Add new section's html
            $('div.section-tabs-left ul').children().removeClass('active');
            $('div.section-tabs-left ul').append(new_section_html);

            var new_section_right_content = "<div class='tab-pane active' id='section_content_" + sectionHtmlID + "'>";
            new_section_right_content = new_section_right_content + "<div class='tabbable page-tabs-right'>";
            new_section_right_content = new_section_right_content + "<ul class='nav nav-pills'></ul></div></div>";
            $('div.notebook-sidebar-right .tab-content').children().removeClass('active');
            $('div.notebook-sidebar-right .tab-content').append(new_section_right_content);

            add_page(e);
            show_success_message("Successfully created section.");
        }
    });
}

function get_associated_tasks_to_note(associations) {
    var taskIDs = [];
    $.each(associations, function (index, association) {
        if (association.associationType == TASK_TO_NOTE) {
            taskIDs.push(association.sourceID);
        }
    });
    return taskIDs;
}

//Create Task for Note Starts
function create_task_from_note(e){
    e = e || window.event;
    var note_html_id = get_internal_id_from_html_id($(e.target).closest('div.panel-default').attr('id'));
    load_task_creation_forNote(note_html_id);
}
function create_task_for_note(){
    var thisNoteID = $('#thisNoteID').val();
    var taskGroups = USR_PROFILE.getTaskGroups()[0];
    var taskGroupID = taskGroups.id;
    var noteTaskTitle = $('#noteTaskTitle').val();
    var noteTaskDueDate = $('#noteTaskDueDate').val();
    var noteTaskPriority = $('#noteTaskPriority').val();

    var convertDueDate = moment(noteTaskDueDate, "MM/DD/YYYY").utc().valueOf() + MILLISECONDS_DAY - 1;

    var newNoteTask = new Object();
    newNoteTask.title = noteTaskTitle;

    newNoteTask.dueDate = convertDueDate;
    newNoteTask.priority = noteTaskPriority;
    var map = new Map();
    map.set("{taskGroupID}", taskGroupID);
    create_task_call(map, newNoteTask, false, function (task) {
        //console.log(task);
        if (task instanceof DigitaryWebError) {
            //console.log("Cannot Create Task: " + task.error.causeMessage);
            show_error_message("Could not create task for note.", task);
        } else {
            var taskHtmlID = task.id;
            //console.log("Task successfully added");
            var taskID = get_html_id_from_internal_id(taskHtmlID);

            var taskTable = "<tr id='" + taskID + "'><td style='color: #3B5998;'>" + get_task_priority_name(task.priority) + "</td><td>" + task.title + "</td>" +
                "<td>" + get_js_date(task.dueDate) + "</td><td></td><td><a href='#' class='task-edit' style='padding-right: 6%;' onclick='javascript: load_associative_editor_task(event);'>" +
                "<i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>" +
                "<a href='#' class='create-note-forTask' style='padding-right: 6%;' onclick='javascript: load_note_editor_from_task(event);'>" +
                "<i class='fa fa-file-text' aria-hidden='true'></i></a>" +
                "<a href='#' class='task-delete' style='padding-right: 6%;' onclick='javascript: delete_task(event);'>" +
                "<i class='fa fa-trash' aria-hidden='true'></i></a></td></tr>";

            //$('#table-task-active tbody tr:first').before(taskTable);
            $('#table-task-active').DataTable().row.add($(taskTable)).draw();
            //create association now

            var newAssociationNoteToTask = new Object();
            newAssociationNoteToTask.sourceID = task.id;
            newAssociationNoteToTask.sinkID = thisNoteID;
            newAssociationNoteToTask.createdTime = moment().utc().valueOf();
            newAssociationNoteToTask.permissionLevelID = 2;
            //console.log(newNoteToTask);
            create_association_call(newAssociationNoteToTask, true, function (association) {
                if (association instanceof DigitaryWebError) {
                    //console.log("Cannot create note: " + association.error.causeMessage);
                    show_error_message("Could not create association.",association);
                }
                else {
                    show_success_message("Successfully created task and associated it to note.");
                }
            });
        }
    });

}

function load_task_creation_forNote(noteID){
    var templateData = new Object();
    templateData.noteID = noteID;
    // Load the html template file
    var html_template = get_inner_HTML("templates/notebook/create-task-from-note.html");
    var template = _.template(html_template);
    load_base_modal("Create task:");
    show_data_on_base_modal(template(templateData));
    $('#noteTaskDueDate').datepicker();
}

function manipulate_label_note(){
    $('#at-label-note').tagit({
        availableTags: USR_PROFILE.getLabels(),
        showAutocompleteOnFocus: true,
        afterTagAdded: function (evt, ui) {
            if (!ui.duringInitialization) {
                var labelName = $('#at-label-note').tagit('tagLabel', ui.tag);
                var label = USR_PROFILE.getLabelByName(labelName);

                var association = new Object();
                association.sourceID = $('#aet-entity').attr('value');
                association.sinkID = label.id;

                // Call to API to add label
                create_association_call(association, true, function (association) {

                    if (association instanceof DigitaryWebError) {
                        show_message_on_base_modal(association);
                        show_error_message("Could not assign label.",association);
                    }
                    else {
                        show_message_on_base_modal('<b>Added Label:</b> ' + labelName);
                        // Now add this to the outer task list table
                        var noteHtmlID = get_html_id_from_internal_id(association.sourceID);
                        var newLabelHtml = "<span class='label label-as-badge label-warning tags' onclick='javascript: load_tagged_entities_by_label_name(event);'>";
                        newLabelHtml = newLabelHtml + labelName + "</span>";
                        $('#'+noteHtmlID).find('li.show-label').append(newLabelHtml);
                        show_success_message("Label successfully associated.");
                    }
                });
            }
        },
        afterTagRemoved: function(evt, ui) {
            var labelName = $('#at-label-note').tagit('tagLabel', ui.tag);
            var label = USR_PROFILE.getLabelByName(labelName);

            var sourceID = $('#aet-entity').attr('value');
            var sinkID = label.id;

            delete_association_call(true, sourceID, sinkID, function (association) {
                if (association instanceof DigitaryWebError) {
                    show_message_on_base_modal(association);
                    show_error_message("Could not delete label.", association);
                }
                else {
                    show_message_on_base_modal('<b>Removed Label:</b> ' + labelName);
                    var noteHtmlID = get_html_id_from_internal_id(association.sourceID);
                    $('#' + noteHtmlID).find("li.show-label span:contains(" + labelName + ")").remove();
                    show_success_message("Successfully deleted label.");
                }
            });
        }
    });
}

function load_associative_editor_note(e) {
    e = e || window.event;
    var note_html_id = $(e.target).closest('div.panel-default').attr('id');
    var noteID= get_internal_id_from_html_id(note_html_id);

    var map = new Map();
    map.set("{entityID}", noteID);

    load_base_modal("Associative Editor for: <b>Note</b>");
    // once the modal is loaded, I will get data.

    get_associated_entities_call(map, true, function(association) {
        var templateData = new Object();
        // Load the html template file
        var html_template = get_inner_HTML("templates/notebook/associative-editor-note.html");
        var template = _.template(html_template);

        show_data_on_base_modal(template(templateData));
        // Save the taskID in a placeholder
        $('#aet-entity').val(association.entity.id);

        templateData = new Object();
        templateData.note = association.entity;
        html_template = get_inner_HTML("templates/notebook/show-note.html");
        template = _.template(html_template);
        $('#note-details').append(template(templateData));
        //Manipulate labels call
        manipulate_label_note();

        templateData = new Object();
        templateData.taskList = association.taskList;
        html_template = get_inner_HTML("templates/common/list-task.html");
        template = _.template(html_template);
        $('#existing-task-association div.clearfix:first-child').append(template(templateData));

        templateData = new Object();
        templateData.messageList = association.emailMessageList;
        html_template = get_inner_HTML("templates/common/list-message.html");
        template = _.template(html_template);
        $('#existing-email-association div.clearfix:first-child').append(template(templateData));
    });
}

function remove_association_note_to_task(e) {
    var taskHtmlID = $('#existing-task-association').find('div.tab-pane.active').attr('id');
    // This value is "aet_<htmlid of note>". So we substring aet_
    taskHtmlID = taskHtmlID.substr(4);
    if(taskHtmlID) {
        var taskID = get_internal_id_from_html_id(taskHtmlID);
        var noteID = $('#aet-entity').val();
        var association = {};
        association.sourceID = taskID;
        association.sinkID = noteID;
        delete_association_call(true, taskID, noteID, function (association) {
            if (association instanceof DigitaryWebError) {
                show_message_on_base_modal(association);
                show_error_message("Could not delete association.", association);
            } else {
                // Add this from existing associations UI
                var id = $('#existing-task-association').find('div.tab-pane.active').attr('id');
                $('#' + id).remove();
                $("a[href='#"+ id + "']").closest('tr').remove();
                // Show message
                show_message_on_base_modal("<b>Successfully</b> removed association of task to the note.");
                show_success_message("Successfully deleted association.");
            }
        });
    }
}
function remove_association_note_to_email(e) {
    var emailHtmlID = $('#existing-email-association').find('div.tab-pane.active').attr('id');
    // This value is "aet_<htmlid of note>". So we substring aet_
    emailHtmlID = emailHtmlID.substr(4);
    if(emailHtmlID) {
        var emailMessageID = get_internal_id_from_html_id(emailHtmlID);
        var noteID = $('#aet-entity').val();

        delete_association_call(true, emailMessageID, noteID, function (association) {
            if (association instanceof DigitaryWebError) {
                show_message_on_base_modal(association);
                show_error_message("Could not delete association.", association);
            } else {
                // Add this from existing associations UI
                var id = $('#existing-email-association').find('div.tab-pane.active').attr('id');
                $('#' + id).remove();
                $("a[href='#"+ id + "']").closest('tr').remove();
                // Show message
                show_message_on_base_modal("<b>Successfully</b> removed association of email to the note.");
                show_success_message("Successfully deleted association.");
            }
        });
    }
}

function search_tasks(e) {
    var q = $('#search-tasks').val();
    get_search_call(q, ENTITY_TASK, false, function(search_result) {
        var templateData = new Object();
        templateData.taskList = search_result.taskList;
        //templateData.totalCount = search_result.totalResultCount;
        // Load the html template file
        var html_template = get_inner_HTML("templates/common/list-task.html");
        var template = _.template(html_template);
        $('#aet-task-search').empty();
        $('#aet-task-search').append(template(templateData));
    });
}

function create_association_note_to_task(e) {
    var taskHtmlID = $('#aet-task-search').find('div.tab-pane.active').attr('id');
    // This value is "aet_<htmlid of note>". So we substring aet_
    taskHtmlID = taskHtmlID.substr(4);
    if(taskHtmlID) {
        var taskID = get_internal_id_from_html_id(taskHtmlID);
        var noteID = $('#aet-entity').val();
        var association = {};
        association.sourceID = taskID;
        association.sinkID = noteID;
        create_association_call(association, true, function (association) {
            if (association instanceof DigitaryWebError) {
                show_message_on_base_modal(association);
                show_error_message("Could not create association.", association);
            } else {
                // Add this to existing associations UI
                var id = $('#aet-task-search').find('div.tab-pane.active').attr('id');
                var text = $('#' + id).html();
                text = "<div class='tab-pane' id='" + id + "'>" + text + "</div>";
                $('#' + id).remove();

                var ahref = $("a[href='#"+ id + "']").closest('tr').detach();
                // Add these 2
                $('#existing-task-association').find('div.tab-content').append(text);
                ahref.appendTo('#existing-task-association table.list-task tbody');
                // Show message
                show_message_on_base_modal("<b>Successfully</b> added association of task to the note.");
                show_success_message("Successfully added association.");
            }
        });
    }
}
function create_association_note_to_email(e) {
    var emailHtmlID = $('#aet-email-search').find('div.tab-pane.active').attr('id');
    // This value is "aet_<htmlid of note>". So we substring aet_
    emailHtmlID = emailHtmlID.substr(4);
    if(emailHtmlID) {
        var emailMessageID = get_internal_id_from_html_id(emailHtmlID);
        var noteID = $('#aet-entity').val();
        var association = {};
        association.sourceID = emailMessageID;
        association.sinkID = noteID;
        create_association_call(association, true, function (association) {
            if (association instanceof DigitaryWebError) {
                show_message_on_base_modal(association);
                show_error_message("Could not create association.", association);
            } else {
                // Add this to existing associations UI
                var id = $('#aet-email-search').find('div.tab-pane.active').attr('id');
                var text = $('#' + id).html();
                text = "<div class='tab-pane' id='" + id + "'>" + text + "</div>";
                $('#' + id).remove();

                var ahref = $("a[href='#"+ id + "']").closest('tr').detach();
                // Add these 2
                $('#existing-email-association').find('div.tab-content').append(text);
                ahref.appendTo('#existing-email-association table.list-email tbody');
                // Show message
                show_message_on_base_modal("<b>Successfully</b> added association of email to the task.");
                show_success_message("Successfully added association.");
            }
        });
    }
}

function edit_section_or_page(type) {
    var e = window.event;
    e.stopPropagation();
    var templateData = new Object();
    templateData.type = type;
    templateData.notebookID = USR_PROFILE.getNotebooks()[0].id;
    if(type == ENTITY_NB_SECTION) {
        templateData.sectionID = get_internal_id_from_html_id($(e.target).parent().attr('id'));
        var sectionName = $(e.target).text().trim();
        templateData.sectionName = sectionName;
        load_base_modal_sm("Edit notebook section: <b>" + sectionName + "</b>");
    } else {
        templateData.pageID = get_internal_id_from_html_id($(e.target).parent().attr('id'));
        var pageName = $(e.target).text().trim();
        templateData.pageName = pageName;
        // Now this section id is in form 'section_content_<id>'
        var sectionHtmlID = $(e.target).closest('div.tab-pane.active').attr('id').substring(16);
        var sectionID = get_internal_id_from_html_id(sectionHtmlID);
        templateData.sectionID = sectionID;
        load_base_modal_sm("Edit notebook section page: <b>" + pageName + "</b>");
    }

    // Load the html template file
    var html_template = get_inner_HTML("templates/notebook/edit-section-page.html");
    var template = _.template(html_template);
    show_data_on_base_modal_sm(template(templateData));
}
//update section name
function update_section_name(){
    var notebookID = $('#notebook-id').val();
    var sectionID = $('#notebook-section-id').val();

    var newSectionName = new Object();
    newSectionName.sectionName = $('#section-section-name').val();
    var map = new Map();
    map.set("{notebookID}", notebookID);
    map.set("{xpath}", sectionID);
    //call update section name
    put_update_notebook_section_call(map, newSectionName, true, function (sectionName) {
        if (sectionName instanceof DigitaryWebError) {
            //console.log("Cannot Create Note: " + sectionName.error.causeMessage);
            show_error_message("Could not update section name.", sectionName);
        } else {
            //location.reload();
            $('div.section-tabs-left').find('li.active a').text(sectionName.sectionName);
            show_success_message("Successfully updated section name.");
        }
    });
}
//delete section name
function delete_section_name(){
    var notebookID = $('#notebook-id').val();
    var sectionID = $('#notebook-section-id').val();

    var map = new Map();
    map.set("{notebookID}", notebookID);
    map.set("{xpath}", sectionID);
    //call update section name
    delete_notebook_section_call(map, true, function (sectionName) {
        if (sectionName instanceof DigitaryWebError) {
            //console.log("Cannot delete section: " + sectionName.error.causeMessage);
            show_error_message("Could not delete notebook section.", sectionName);
        } else {
            //location.reload();
            $('div.section-tabs-left').find('li.active').remove();
            show_success_message("Successfully deleted section.");
        }
    });
}
//update page name
function update_page_name(){
    var notebookID = $('#notebook-id').val();
    var sectionID = $('#notebook-section-id').val();
    var pageID = $('#notebook-page-id').val();

    var newPageName = new Object();
    newPageName.pageName = $('#section-page-name').val();

    var map = new Map();
    map.set("{notebookID}", notebookID);
    map.set("{xpath}", sectionID);
    map.set("{pageID}", pageID);
    //call update page name
    put_update_notebook_section_page_call(map, newPageName, true, function(pageName){
        if (pageName instanceof DigitaryWebError) {
            //console.log("Cannot Create Note: " + pageName.error.causeMessage);
            show_error_message("Could not update page name.", pageName);
        } else {
            $('.notebook-sidebar-right').children('div.tab-content').children('div.tab-pane.active').find('li.active a').text(pageName.pageName);
            show_success_message("Successfully updated page name.");
        }
    });
}
//delete page name
function delete_page_name(){
    var notebookID = $('#notebook-id').val();
    var sectionID = $('#notebook-section-id').val();
    var pageID = $('#notebook-page-id').val();

    var map = new Map();
    map.set("{notebookID}", notebookID);
    map.set("{xpath}", sectionID);
    map.set("{pageID}", pageID);
    //call update section name
    delete_notebook_page_call(map, true, function (pageName) {
        if (pageName instanceof DigitaryWebError) {
            //console.log("Cannot delete page: " + pageName.error.causeMessage);
            show_error_message("Could not delete notebook page.", pageName);
        } else {
            //location.reload();
            $('div.page-tabs-right').find('li.active').remove();
            //$('.notebook-sidebar-right').children('div.tab-content').children('div.tab-pane.active').find('li.active a').delete(pageName.pageName);
            show_success_message("Successfully deleted page.");
        }
    });
}