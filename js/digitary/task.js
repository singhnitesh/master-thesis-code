/**
 * Created by Digitay Development Team: Siddhartha Moitra, Nitesh Singh on 06.05.16.
 */

function get_task_status_string(taskStatus) {
    if (taskStatus == 1)
        return 'OPEN';
    else
        return 'COMPLETE';
}

function get_task_priority_string(taskPriority) {
    if (taskPriority == 1)
        return 'HIGH';
    else if (taskPriority == 2)
        return 'MEDIUM';
    else if (taskPriority == 3)
        return 'LOW';
    else if (taskPriority == 4)
        return 'TRIVIAL';
}

function init_task_base() {
    $('#dueDate').datepicker({
        minDate: 0
    });

    $('#table-task-overdue').DataTable({
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false,
        }]
    });

    $('#table-task-active').DataTable({
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false,
        }]
    });
    $('#table-task-complete').DataTable({
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false,
        }]
    });
}
function delete_task(e) {
    e = e || window.event;
    var taskhtmlid = $(e.target).closest('tr').attr('id');
    var taskID = get_internal_id_from_html_id(taskhtmlid);

    var map = new Map();
    map.set("{taskID}", taskID);
    delete_task_call(map, true, function (task) {
        if (task instanceof DigitaryWebError) {
            //console.log('Cannot delete Task' + task.error.causeMessage);
            show_error_message("Could not delete task.", task);
        }
        else {
            //console.log('task successfully deleted')
            var tableId = $('#task-container').find('div.tab-pane.active').attr('id');
            var table = $('#table-' + tableId).DataTable();
            table.row($('#' + taskhtmlid)).remove().draw();
            show_success_message("Successfully deleted task.");
        }
    });

}
function update_task() {
    var title = $('#mTaskTitle').val();
    var due = $('#mTaskDue').val();
    var status = $('#mTaskStatus').val();
    var priority = $('#mTaskPriority').val();
    // validate
    if (title.trim() == '') {
        show_message_on_base_modal('<b>Error:</b> Task Title cannot be empty');
        return false;
    }
    var task = {};
    task.title = title;
    task.taskBody = '';
    task.priority = priority;
    task.status = status;
    task.dueDate = moment(due, "MM/DD/YYYY").utc().valueOf() + MILLISECONDS_DAY - 1;

    var data_map = new Map();
    data_map.set("{taskID}", $('#mTaskId').val());
    update_task_call(data_map, task, true, function (updated_task) {
        if (updated_task instanceof DigitaryWebError) {
            show_message_on_base_modal('<b>Error:</b> Could not update task. Reason: ' + updated_task.error.apiMessage);
            show_error_message('Could not update task', updated_task);
        } else {
            show_message_on_base_modal('<b>Success:</b> Task updated successfully.');
            // remove the existing task first and then we add the updated task
            var taskHtmlId = get_html_id_from_internal_id(updated_task.id);
            var table_active = 'table-' + $('#task-container .tab-pane.active').attr('id');
            var dTable = $('#' + table_active).DataTable();
            dTable.row('#' + taskHtmlId).remove().draw();

            var dTableNew = 'table-';
            var taskRow = '';

            if(updated_task.status == TASK_OPEN) {
                taskRow = "<tr id='" + taskHtmlId + "'><td style='color: #3B5998;'>";
                dTableNew = dTableNew + 'task-active';
                show_success_message('Task status updated.');
            } else if(updated_task.status == TASK_COMPLETE) {
                taskRow = "<tr id='" + taskHtmlId + "'><td style='color: #1ABC9C;'>";
                dTableNew = dTableNew + 'task-complete';
                show_success_message('Task status updated.');
            }
            taskRow = taskRow  + get_task_priority_name(updated_task.priority) + "</td><td>" + updated_task.title + "</td>" +
                "<td>" + get_js_date(updated_task.dueDate) + "</td><td>";
            $.each(updated_task.labels, function (index, label) {
                taskRow = taskRow + "<span class='label label-as-badge label-warning tags' onclick='javascript: load_tagged_entities_by_label_name(event);'>" + label.labelName + "</span>";
            });
            taskRow = taskRow + "</td><td><a href='#' class='task-edit' style='padding-right: 6%;' onclick='javascript: load_associative_editor_task(event);'>" +
                "<i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>" +
                "<a href='#' class='create-note-forTask' style='padding-right: 6%;' onclick='javascript: load_note_editor_from_task(event);'>" +
                "<i class='fa fa-file-text' aria-hidden='true'></i></a>" +
                "<a href='#' class='task-delete' style='padding-right: 6%;' onclick='javascript: delete_task(event);'>" +
                "<i class='fa fa-trash' aria-hidden='true'></i></a></td></tr>";

            $('#'+dTableNew).DataTable().row.add($(taskRow)).draw();
            show_success_message('Task successfully updated');
        }
    });

    return false;
}

function load_associative_editor_task(e) {
    e = e || window.event;
    // Get Task HTML ID from grandparent <tr>
    var task_html_id = $(e.target).closest('tr').attr('id');
    var taskID = get_internal_id_from_html_id(task_html_id);

    var map = new Map();
    map.set("{entityID}", taskID);

    load_base_modal("Associative Editor for: <b>Task</b>");

    get_associated_entities_call(map, true, function (association) {
        var templateData = new Object();
        // Load the html template file
        var html_template = get_inner_HTML("templates/task/associative-editor-task.html");
        var template = _.template(html_template);

        show_data_on_base_modal(template(templateData));
        // Save the taskID in a placeholder
        $('#aet-entity').val(association.entity.id);

        templateData = new Object();
        templateData.task = association.entity;
        html_template = get_inner_HTML("templates/task/show-editable-task.html");
        template = _.template(html_template);
        $('#task-details').append(template(templateData));
        // Assign date picker
        $('#mTaskDue').datepicker({
            minDate: 0
        });
        //Manipulate labels call
        manipulate_label_task();

        templateData = new Object();
        templateData.noteList = association.noteList;
        html_template = get_inner_HTML("templates/common/list-note.html");
        template = _.template(html_template);
        $('#existing-note-association div.clearfix:first-child').append(template(templateData));

        templateData = new Object();
        templateData.messageList = association.emailMessageList;
        html_template = get_inner_HTML("templates/common/list-message.html");
        template = _.template(html_template);
        $('#existing-email-association div.clearfix:first-child').append(template(templateData));
    });
}

//Manipulate task
function manipulate_label_task() {
    $('#at-label-task').tagit({
        availableTags: USR_PROFILE.getLabels(),
        showAutocompleteOnFocus: true,
        afterTagAdded: function (evt, ui) {
            if (!ui.duringInitialization) {
                var labelName = $('#at-label-task').tagit('tagLabel', ui.tag);
                var label = USR_PROFILE.getLabelByName(labelName);

                var association = new Object();
                association.sourceID = $('#aet-entity').attr('value');
                association.sinkID = label.id;

                // Call to API to add label
                create_association_call(association, true, function (association) {
                    if (association instanceof DigitaryWebError) {
                        show_message_on_base_modal(association);
                        show_error_message("Could not assign label.", association);
                    } else {
                        show_message_on_base_modal('<b>Added Label:</b> ' + labelName);
                        // Now add this to the outer task list table
                        var taskHtmlID = get_html_id_from_internal_id(association.sourceID);
                        var newLabelHtml = "<span class='label label-as-badge label-warning tags' onclick='javascript: load_tagged_entities_by_label_name(event);'>";
                        newLabelHtml = newLabelHtml + labelName + "</span>";
                        $('#' + taskHtmlID).children('td:nth-child(4)').append(newLabelHtml);
                        show_success_message("Successfully assigned label");
                    }
                });
            }
        },
        afterTagRemoved: function (evt, ui) {
            var labelName = $('#at-label-task').tagit('tagLabel', ui.tag);
            var label = USR_PROFILE.getLabelByName(labelName);

            var sourceID = $('#aet-entity').attr('value');
            var sinkID = label.id;

            delete_association_call(true, sourceID, sinkID, function (association) {
                if (association instanceof DigitaryWebError) {
                    show_message_on_base_modal(association);
                    show_error_message("Could not delete label.", association);
                } else {
                    show_message_on_base_modal('<b>Removed Label:</b> ' + labelName);
                    var taskHtmlID = get_html_id_from_internal_id(association.sourceID);
                    $('#' + taskHtmlID).find("span:contains(" + labelName + ")").remove();
                    show_success_message("Successfully deleted label.");
                }
            });
        }
    });
}
function add_new_task(e) {
    e = e || window.event;
    var taskGroup = USR_PROFILE.getTaskGroups()[0];
    var taskGroupID = taskGroup.id;

    var title = $('#inputAddTask').val();
    var taskduedate = $('#dueDate').val().split('/');
    var convertDueDate = moment(taskduedate, "MM/DD/YYYY").utc().valueOf() + MILLISECONDS_DAY - 1;
    var newTask = new Object();
    newTask.title = title;
    newTask.dueDate = convertDueDate;

    var map = new Map();
    map.set("{taskGroupID}", taskGroupID);
    create_task_call(map, newTask, false, function (task) {
        if (task instanceof DigitaryWebError) {
            //console.log("Cannot Create Task: " + task.error.causeMessage);
            show_error_message("Could not create task.", task);
        } else {
            var taskHtmlID = task.id;
            var taskID = get_html_id_from_internal_id(taskHtmlID);
            var taskRow = "<tr id='" + taskID + "'><td style='color: #3B5998;'>" + get_task_priority_name(task.priority) + "</td><td>" + task.title + "</td>" +
                "<td>" + get_js_date(task.dueDate) + "</td><td></td><td><a href='#' class='task-edit' style='padding-right: 6%;' onclick='javascript: load_associative_editor_task(event);'>" +
                "<i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>" +
                "<a href='#' class='create-note-forTask' style='padding-right: 6%;' onclick='javascript: load_note_editor_from_task(event);'>" +
                "<i class='fa fa-file-text' aria-hidden='true'></i></a>" +
                "<a href='#' class='task-delete' style='padding-right: 6%;' onclick='javascript: delete_task(event);'>" +
                "<i class='fa fa-trash' aria-hidden='true'></i></a></td></tr>";

            var t = $('#table-task-active').DataTable();
            t.row.add($(taskRow)).draw();
            show_success_message("Successfully created task.");
        }
        $('#taskCreationForm').trigger("reset");
    });

}

function create_association_task_to_note(e) {
    var noteHtmlID = $('#aet-note-search').find('div.tab-pane.active').attr('id');
    // This value is "aet_<htmlid of note>". So we substring aet_
    noteHtmlID = noteHtmlID.substr(4);
    if (noteHtmlID) {
        var noteID = get_internal_id_from_html_id(noteHtmlID);
        var taskID = $('#aet-entity').val();
        var association = {};
        association.sourceID = taskID;
        association.sinkID = noteID;
        create_association_call(association, true, function (association) {
            if (association instanceof DigitaryWebError) {
                show_message_on_base_modal(association);
                show_error_message("Could not create association.", association);
            } else {
                // Add this to existing associations UI
                var id = $('#aet-note-search').find('div.tab-pane.active').attr('id');
                var text = $('#' + id).html();
                text = "<div class='tab-pane' id='" + id + "'>" + text + "</div>";
                $('#' + id).remove();

                var ahref = $("a[href='#" + id + "']").closest('tr').detach();
                // Add these 2
                $('#existing-note-association').find('div.tab-content').append(text);
                ahref.appendTo('#existing-note-association table.list-note tbody');
                // Show message
                show_message_on_base_modal("<b>Successfully</b> added association of note to the task.");
                show_success_message("Successfully added association.");
            }
        });
    }
}

function create_association_task_to_email(e) {
    var emailHtmlID = $('#aet-email-search').find('div.tab-pane.active').attr('id');
    // This value is "aet_<htmlid of note>". So we substring aet_
    emailHtmlID = emailHtmlID.substr(4);
    if (emailHtmlID) {
        var emailMessageID = get_internal_id_from_html_id(emailHtmlID);
        var taskID = $('#aet-entity').val();
        var association = {};
        association.sourceID = emailMessageID;
        association.sinkID = taskID;
        create_association_call(association, true, function (association) {
            if (association instanceof DigitaryWebError) {
                show_message_on_base_modal(association);
                show_error_message("Could not create association", association);
            } else {
                // Add this to existing associations UI
                var id = $('#aet-email-search').find('div.tab-pane.active').attr('id');
                var text = $('#' + id).html();
                text = "<div class='tab-pane' id='" + id + "'>" + text + "</div>";
                $('#' + id).remove();

                var ahref = $("a[href='#" + id + "']").closest('tr').detach();
                // Add these 2
                $('#existing-email-association').find('div.tab-content').append(text);
                ahref.appendTo('#existing-email-association table.list-email tbody');
                // Show message
                show_message_on_base_modal("<b>Successfully</b> added association of email to the task.");
                show_success_message("Successfully created association.");
            }
        });
    }
}

function remove_association_task_to_note(e) {
    var noteHtmlID = $('#existing-note-association').find('div.tab-pane.active').attr('id');
    // This value is "aet_<htmlid of note>". So we substring aet_
    noteHtmlID = noteHtmlID.substr(4);
    if (noteHtmlID) {
        var noteID = get_internal_id_from_html_id(noteHtmlID);
        var taskID = $('#aet-entity').val();

        delete_association_call(true, taskID, noteID, function (association) {
            if (association instanceof DigitaryWebError) {
                show_message_on_base_modal(association);
                show_error_message("Could not delete association.", association);
            } else {
                // Add this from existing associations UI
                var id = $('#existing-note-association').find('div.tab-pane.active').attr('id');
                $('#' + id).remove();
                $("a[href='#" + id + "']").closest('tr').remove();
                // Show message
                show_message_on_base_modal("<b>Successfully</b> removed association of note to the task.");
                show_success_message("Successfully deleted association.");
            }
        });
    }
}
function remove_association_task_to_email(e) {
    var emailHtmlID = $('#existing-email-association').find('div.tab-pane.active').attr('id');
    // This value is "aet_<htmlid of note>". So we substring aet_
    emailHtmlID = emailHtmlID.substr(4);
    if (emailHtmlID) {
        var emailMessageID = get_internal_id_from_html_id(emailHtmlID);
        var taskID = $('#aet-entity').val();

        delete_association_call(true, emailMessageID, taskID, function (association) {
            if (association instanceof DigitaryWebError) {
                show_message_on_base_modal(association);
                show_error_message("Could not delete association.", association);
            } else {
                // Add this from existing associations UI
                var id = $('#existing-email-association').find('div.tab-pane.active').attr('id');
                $('#' + id).remove();
                $("a[href='#" + id + "']").closest('tr').remove();
                // Show message
                show_message_on_base_modal("<b>Successfully</b> removed association of email to the task.");
                show_success_message("Successfully deleted association.");
            }
        });
    }
}

function search_notes(e) {
    var q = $('#search-notes').val();
    get_search_call(q, ENTITY_NOTE, false, function (search_result) {
        var templateData = new Object();
        templateData.noteList = search_result.noteList;
        //templateData.totalCount = search_result.totalResultCount;
        // Load the html template file
        var html_template = get_inner_HTML("templates/common/list-note.html");
        var template = _.template(html_template);
        $('#aet-note-search').empty();
        $('#aet-note-search').append(template(templateData));
    });
}

function search_emails(e) {
    var q = $('#search-emails').val();
    get_search_call(q, ENTITY_EMAIL, false, function (search_result) {
        var templateData = new Object();
        var emailMessageList = [];
        // This returns with a list of conversations
        $.each(search_result, function (index, conversation) {
            $.each(conversation.messageList, function (index, message) {
                emailMessageList.push(message);
            });
        });
        templateData.messageList = emailMessageList;
        //templateData.totalCount = search_result.totalResultCount;
        // Load the html template file
        var html_template = get_inner_HTML("templates/common/list-message.html");
        var template = _.template(html_template);
        $('#aet-email-search').empty();
        $('#aet-email-search').append(template(templateData));
    });
}

function load_note_editor_from_task(e) {
    e = e || window.event;
    var taskHtmlID = $(e.target).closest('tr').attr('id');
    var taskID = get_internal_id_from_html_id(taskHtmlID);
    var notebook = USR_PROFILE.getNotebooks()[0];

    var templateData = new Object();
    templateData.taskID = taskID;
    templateData.notebook = notebook;
    // Load the html template file
    var html_template = get_inner_HTML("templates/task/create-note.html");
    var template = _.template(html_template);
    load_base_modal("Create note");
    show_data_on_base_modal(template(templateData));
    // Associate TinyMCE editor
    associate_tinymce_editor_notinline_with_id('create-note-body-task-modal');
    // Add all event handlers for this modal
    add_event_handler_task_note_editor();
}

function add_event_handler_task_note_editor() {
    // Code to flush modal from DOM completely
    $("#base-modal").on('hidden.bs.modal', function () {
        var editorID = tinymce.get('create-note-body-task-modal');
        //alert(editorID);
        tinymce.execCommand('mceRemoveEditor', true, editorID);
    });
    if (tinymce.editors.length > 0) {
        tinymce.execCommand('mceFocus', true, 'create-note-body-task-modal');
        tinymce.execCommand('mceRemoveEditor', true, 'create-note-body-task-modal');
        tinymce.execCommand('mceAddEditor', true, 'create-note-body-task-modal');
    }

    $('#select-noteBook-section').change(function () {

        var selectedSection = $(this).find(":selected").val();
        $('#select-section-page option').each(function () {
            var self = $(this);
            if (self.hasClass(selectedSection)) {
                self.addClass('show');
                self.removeClass('hide');
            } else {
                self.addClass('hide');
                self.removeClass('show');
            }
        });

    });
}

function create_note_from_task() {
    var content_html = tinyMCE.get('create-note-body-task-modal').getContent();

    // Make this MD format
    // TODO
    convert_html_to_markdown(content_html, function (markdown) {
        var notebookID = $('#selected-noteBookID').val();
        var selectedSectionID = get_internal_id_from_html_id($('#select-noteBook-section').find(":selected").val());
        var selectedPageID = get_internal_id_from_html_id($('#select-section-page').find(":selected").val());

        var onSpotNoteForTask = new Object();
        onSpotNoteForTask.noteBody = markdown;
        var map = new Map();
        map.set("{notebookID}", notebookID);
        map.set("{xpath}", selectedSectionID);
        map.set("{pageID}", selectedPageID);
        create_note_call(map, onSpotNoteForTask, true, function (note) {
            if (note instanceof DigitaryWebError) {
                show_message_on_base_modal(note);
                show_error_message('Could not create note.', note);
            }
            else {
                show_message_on_base_modal("<b>Successfully</b> created note. Close Modal to continue your work.");
                var notes = [];
                notes.push(note);
                //add created note on Notebook UI
                var pageID = 'page_content_' + get_html_id_from_internal_id(selectedPageID);

                add_note($('#' + pageID).find('div.page-content'), note);

                //create association now
                var association = new Object();
                association.sourceID = $('#selected-noteTaskID').val();
                association.sinkID = note.id;
                create_association_call(association, true, function (association) {
                    if (association instanceof DigitaryWebError) {
                        show_data_on_base_modal(association);
                        show_error_message('Could not create association', association);
                    }
                    else {
                        show_data_on_base_modal("<b>Successfully</b> associated the note to task.");
                        show_success_message('Successfully created note and associated to task.');
                    }
                });
            }
        });
    });
}