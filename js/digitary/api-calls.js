/**
 * Created by Digitay Development Team: Siddhartha Moitra, Nitesh Singh on 26.04.16.
 */
function get_cookie_access() {
    var cookie_sid = $.cookie(COOKIE_SID);
    if (!cookie_sid)
        throw DigitaryWebError("No cookie set for access token.", null);
    return cookie_sid;
}
/***************************************************************
 * USER PROFILE
 ***************************************************************/
function get_username_availability_call(data_map, async, callback) {
    var final_url = get_final_computable_url("GET_USER_AVAILABLE", data_map);
    // Check if this username is available
    $.ajax({
            url: final_url,
            type: "GET",
            async: async,
            dataType: "json",
            headers: {
                "AppID": APP_ID
            }
        })
        .done(function (data, textStatus, jqXHR) {
            callback(jQuery.parseJSON(JSON.stringify(data)));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            callback(new DigitaryWebError("Cannot check user availability due to server error.", jqXHR));
        })
}
function post_user_registration_call(post_data, async, callback) {
    var final_url = get_final_computable_url("REGISTER_USER", {});
    $.post({
            url: final_url,
            type: "POST",
            async: async,
            data: $.trim(JSON.stringify(post_data)),
            dataType: "json",
            contentType: "application/json",
            headers: {
                "AppID": APP_ID
            }
        })
        .done(function (data, textStatus, jqXHR) {
            callback(jQuery.parseJSON(JSON.stringify(data)));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            callback(new DigitaryWebError("Cannot register user due to server error.", jqXHR));
        })
}

function get_user_login_call(base64, async, callback) {
    var final_url = get_final_computable_url("GET_USER_LOGIN", {});
    // Send AJAX request to login
    ////console.log('Getting user login at URL: ' + final_url);
    $.ajax({
            url: final_url,
            type: "GET",
            async: async,
            dataType: "json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Basic " + base64
            }
        })
        .done(function (data, textStatus, jqXHR) {
            callback(jQuery.parseJSON(JSON.stringify(data)));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            callback(new DigitaryWebError("Cannot login due to server response.", jqXHR));
        })
}

function get_user_logout_call(data_map, async) {
    var cookie_sid = get_cookie_access();
    $.ajax({
            url: get_final_computable_url("GET_USER_LOGOUT", data_map),
            type: "DELETE",
            async: async,
            dataType: "json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Token " + cookie_sid
            }
        })
        .done(function (data, textStatus, jqXHR) {
            return jQuery.parseJSON(JSON.stringify(data));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            throw DigitaryWebError("1", jqXHR);
        })
}
function get_cookie_validate_call(cookie_value, callback) {
    var final_url = get_final_computable_url("GET_BEARER_AUTHENTICATION", {});
    // Send cookie value as AJAX to API
    $.ajax({
            url: final_url,
            type: "GET",
            async: false,
            dataType: "json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Bearer " + cookie_value
            }
        })
        .done(function (data, textStatus, jqXHR) {
            // Callback
            callback(new Boolean(jQuery.parseJSON(JSON.stringify(data))));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            callback(new DigitaryWebError("Cannot validate cookie due to API error", jqXHR));
        })
}

function get_user_profile_call(data_map, async, callback) {
    var cookie_sid = get_cookie_access();
    var final_url = get_final_computable_url("GET_USER_PROFILE", data_map);
    ////console.log('Getting user profile at URL: ' + final_url);
    if (cookie_sid) {
        $.ajax({
                url: final_url,
                type: "GET",
                async: async,
                dataType: "json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot get user profile due to server error.", jqXHR));
            })
    }
}
//DELETE USER PROFILE
function delete_user_profile_call(data_map, async, callback) {
    var cookie_sid = get_cookie_access();
    var final_url = get_final_computable_url("DELETE_USER_PROFILE", data_map);
    //console.log('Getting user profile at URL: ' + final_url);
    if (cookie_sid) {
        $.ajax({
            url: final_url,
            type: "DELETE",
            async: async,
            dataType: "json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Token " + cookie_sid
            }
        })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot get user profile due to server error.", jqXHR));
            })
    }
}
/***************************************************************
 * NOTEBOOK
 ***************************************************************/
function get_notebook_hierarchy_call(data_map, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("GET_NOTEBOOK_HIERARCHY", data_map);
        ////console.log('Getting notebook hierarchy at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.ajax({
                url: final_url,
                type: "GET",
                async: async,
                dataType: "json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot retrieve notebook hierarchy due to server error.", jqXHR));
            })
    }
}


function get_notebook_section_page_notes_call(data_map, async, callback) {
    var cookie_sid = get_cookie_access();
    var final_url = get_final_computable_url("GET_NOTEBOOK_PAGE_NOTES", data_map);
    ////console.log("Fetching notes from URL: " + final_url);
    // Send an AJAX request to fetch notes
    $.ajax({
            url: final_url,
            type: "GET",
            async: async,
            dataType: "json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Token " + cookie_sid
            }
        })
        .done(function (data, textStatus, jqXHR) {
            //here is the call for indexDB
            var notes = jQuery.parseJSON(JSON.stringify(data));
            callback(notes);
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            callback(new DigitaryWebError("Cannot retrieve notebook page notes due to server error.", jqXHR));
        })
}
//DELETE NOTE CALL
function delete_note_call(data_map, async, callback) {
    // First delete this note AJAX
    var cookie_sid = get_cookie_access();
    var final_url = get_final_computable_url("DELETE_NOTEBOOK_PAGE_NOTE", data_map);
    ////console.log("Deleting a note from URL: " + final_url);
    // Send an AJAX request to fetch notes
    $.ajax({
            url: final_url,
            type: "DELETE",
            async: async,
            dataType: "json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Token " + cookie_sid
            }
        })
        .done(function (data, textStatus, jqXHR) {
            callback(jQuery.parseJSON(JSON.stringify(data)));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            callback(new DigitaryWebError("Cannot delete note due to server error.", jqXHR));
        })
}
//UPDATE NOTE CALL
function update_note_call(data_map, post_data, async, callback) {
    // First delete this note AJAX
    var cookie_sid = get_cookie_access();
    var final_url = get_final_computable_url("UPDATE_NOTEBOOK_PAGE_NOTE", data_map);
    ////console.log("Updating a note from URL: " + final_url);
    // Send an AJAX request to fetch notes
    $.ajax({
            url: final_url,
            type: "PUT",
            async: async,
            data: $.trim(JSON.stringify(post_data)),
            dataType: "json",
            contentType: "application/json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Token " + cookie_sid
            }
        })
        .done(function (data, textStatus, jqXHR) {
            callback(jQuery.parseJSON(JSON.stringify(data)));

        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            callback(new DigitaryWebError("Cannot update note due to server error.", jqXHR));
        })
}
//CREATE NOTE CALL
function create_note_call(data_map, post_data, async, callback) {
    // First delete this note AJAX
    var cookie_sid = get_cookie_access();
    var final_url = get_final_computable_url("CREATE_NOTEBOOK_PAGE_NOTE", data_map);
    ////console.log("Creating a note from URL: " + final_url);
    // Send an AJAX request to fetch notes
    $.post({
            url: final_url,
            type: "POST",
            async: async,
            data: $.trim(JSON.stringify(post_data)),
            dataType: "json",
            contentType: "application/json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Token " + cookie_sid
            }
        })
        .done(function (data, textStatus, jqXHR) {
            var note = jQuery.parseJSON(JSON.stringify(data));
            callback(note);
            //callback(jQuery.parseJSON(JSON.stringify(data)));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            callback(new DigitaryWebError("Cannot create note due to server error.", jqXHR));
        })
}
function put_update_notebook_call(data_map, post_data, async, callback) {
    var cookie_sid = get_cookie_access();
    var final_url = get_final_computable_url("UPDATE_NOTEBOOK", data_map);
    ////console.log("Updating a notebook at: " + final_url);
    // Send an AJAX request to fetch notes
    $.ajax({
            url: final_url,
            type: "PUT",
            async: async,
            data: $.trim(JSON.stringify(post_data)),
            dataType: "json",
            contentType: "application/json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Token " + cookie_sid
            }
        })
        .done(function (data, textStatus, jqXHR) {
            callback(jQuery.parseJSON(JSON.stringify(data)));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            //TODO check in Database
            //callback(post_data);
            callback(new DigitaryWebError("Cannot update notebook details.", jqXHR));
        })
}
//CREATE NOTEBOOK CALL
function create_notebook_call(post_data, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid){
        var final_url = get_final_computable_url("CREATE_NOTEBOOK", {});
        //console.log("Creating a notebook at: " + final_url);
        // Send an AJAX request to fetch notes
        $.post({
            url: final_url,
            type: "POST",
            async: async,
            data: $.trim(JSON.stringify(post_data)),
            dataType: "json",
            contentType: "application/json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Token " + cookie_sid
            }
        })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot create notebook due to server error.", jqXHR));
            })
    }
}
//CREATE PAGE CALL
function create_page_call(data_map, post_data, async, callback) {
    // First delete this note AJAX
    var cookie_sid = get_cookie_access();
    var final_url = get_final_computable_url("CREATE_SECTION_PAGE", data_map);
    //console.log("Creating a page from URL: " + final_url);
    // Send an AJAX request to fetch notes
    $.post({
            url: final_url,
            type: "POST",
            async: async,
            data: $.trim(JSON.stringify(post_data)),
            dataType: "json",
            contentType: "application/json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Token " + cookie_sid
            }
        })
        .done(function (data, textStatus, jqXHR) {
            callback(jQuery.parseJSON(JSON.stringify(data)));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            callback(new DigitaryWebError("Cannot create page due to server error.", jqXHR));
        })
}
//Update section name call
function put_update_notebook_section_call(data_map, post_data, async, callback) {
    var cookie_sid = get_cookie_access();
    var final_url = get_final_computable_url("UPDATE_SECTION", data_map);
    //console.log("Updating a notebook section at: " + final_url);
    // Send an AJAX request to fetch notes
    $.ajax({
            url: final_url,
            type: "PUT",
            async: async,
            data: $.trim(JSON.stringify(post_data)),
            dataType: "json",
            contentType: "application/json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Token " + cookie_sid
            }
        })
        .done(function (data, textStatus, jqXHR) {
            callback(jQuery.parseJSON(JSON.stringify(data)));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            //TODO check in Database
            //callback(post_data);
            callback(new DigitaryWebError("Cannot update notebook section details.", jqXHR));
        })
}
//Update page name call
function put_update_notebook_section_page_call(data_map, post_data, async, callback) {
    var cookie_sid = get_cookie_access();
    var final_url = get_final_computable_url("UPDATE_PAGE", data_map);
    //console.log("Updating a notebook at: " + final_url);
    // Send an AJAX request to fetch notes
    $.ajax({
            url: final_url,
            type: "PUT",
            async: async,
            data: $.trim(JSON.stringify(post_data)),
            dataType: "json",
            contentType: "application/json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Token " + cookie_sid
            }
        })
        .done(function (data, textStatus, jqXHR) {
            callback(jQuery.parseJSON(JSON.stringify(data)));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            //TODO check in Database
            //callback(post_data);
            callback(new DigitaryWebError("Cannot update notebook page details.", jqXHR));
        })
}
//Delete notebook section call
function delete_notebook_section_call(data_map, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("DELETE_SECTION", data_map);
        //console.log('Deleting section at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.ajax({
                url: final_url,
                type: "DELETE",
                async: async,
                dataType: "json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot delete section due to server error.", jqXHR));
            })
    }
}
//Delete notebook page call
function delete_notebook_page_call(data_map, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("DELETE_PAGE", data_map);
        //console.log('Deleting page at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.ajax({
                url: final_url,
                type: "DELETE",
                async: async,
                dataType: "json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot delete page due to server error.", jqXHR));
            })
    }
}
//CREATE PAGE CALL
function create_section_call(data_map, post_data, async, callback) {
    // First delete this note AJAX
    var cookie_sid = get_cookie_access();
    var final_url = get_final_computable_url("CREATE_SECTION", data_map);
    //console.log("Creating a section from URL: " + final_url);
    // Send an AJAX request to fetch notes
    $.post({
            url: final_url,
            type: "POST",
            async: async,
            data: $.trim(JSON.stringify(post_data)),
            dataType: "json",
            contentType: "application/json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Token " + cookie_sid
            }
        })
        .done(function (data, textStatus, jqXHR) {
            callback(jQuery.parseJSON(JSON.stringify(data)));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            callback(new DigitaryWebError("Cannot create section due to server error.", jqXHR));
        })
}
function get_note_call(data_map, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("GET_NOTE", data_map);
        //console.log('Getting note at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.ajax({
                url: final_url,
                type: "GET",
                async: async,
                dataType: "json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot retrieve task hierarchy due to server error.", jqXHR));
            })
    }
}
/***************************************************************
 * TASK GROUP
 ***************************************************************/
function get_task_group_call(data_map, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("GET_TASK_GROUPS", data_map);
        //console.log('Getting task group at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.ajax({
                url: final_url,
                type: "GET",
                async: async,
                dataType: "json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot retrieve task hierarchy due to server error.", jqXHR));
            })
    }
}
//CREATE TASK GROUP CALL
function create_task_group_call(post_data, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("CREATE_TASK_GROUPS", {});
        //console.log('Creating task group at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.post({
                url: final_url,
                type: "POST",
                async: async,
                data: $.trim(JSON.stringify(post_data)),
                dataType: "json",
                contentType: "application/json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot create task group due to server error.", jqXHR));
            })
    }
}

//UPDATE TASK GROUP CALL
function update_task_group_call(data_map, post_data, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("UPDATE_TASK_GROUPS", data_map);
        //console.log('Updating task group at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.ajax({
                url: final_url,
                type: "PUT",
                async: async,
                data: $.trim(JSON.stringify(post_data)),
                dataType: "json",
                contentType: "application/json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot update task group due to server error.", jqXHR));
            })
    }
}
//DELETE TASK GROUP CALL
function delete_task_group_call(data_map, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("DELETE_TASK_GROUPS", data_map);
        //console.log('Deleting task group at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.ajax({
                url: final_url,
                type: "DELETE",
                async: async,
                dataType: "json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot delete task group due to server error.", jqXHR));
            })
    }
}


/***************************************************************
 * LABEL CALLS
 ***************************************************************/
//GET LABELS CALL
function get_label_call(callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("GET_LABELS");//get_final_computable_url("GET_NOTEBOOK_HIERARCHY", data_map);
        //console.log('Getting labels at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.ajax({
                url: final_url,
                type: "GET",
                async: true,
                dataType: "json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                var labels = jQuery.parseJSON(JSON.stringify(data));
                callback(labels);
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot retrieve labels due to server error.", jqXHR));
            })
    }
}
//GET ALL ASSOCIATED ENTITIES WITH LABELS CALL
function get_all_entities_by_labels(labelIDs, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("GET_ENTITIES_BY_LABEL", null);
        final_url = final_url + "?";
        var request_param = '';
        $.each(labelIDs, function (index, labelID) {
            request_param = request_param + "labelID=" + labelID + "&";
        });
        // preprocess this
        request_param = preprocess_html_escape(request_param);
        final_url = final_url + request_param;
        //console.log('Getting all data with label at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.ajax({
                url: final_url,
                type: "GET",
                async: async,
                dataType: "json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));

            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot retrieve entities with label due to server error.", jqXHR));
            })
    }
}
function get_all_entities_by_label(labelID, async, callback) {
    var labelIDs = [];
    labelIDs.push(labelID);
    get_all_entities_by_labels(labelIDs, async, callback);
}
//Create LABELS CALL
function create_label_call(post_data, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("CREATE_LABELS");//get_final_computable_url("GET_NOTEBOOK_HIERARCHY", data_map);
        //console.log('Creating labels at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.post({
                url: final_url,
                type: "POST",
                async: async,
                data: $.trim(JSON.stringify(post_data)),
                dataType: "json",
                contentType: "application/json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));

            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot create labels due to server error.", jqXHR));
            })
    }
}
/***************************************************************
 * ASSOCIATION CALLS
 ***************************************************************/
//CREATE ASSOCIATION CALL
function create_association_call(post_data, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("CREATE_ASSOCIATIONS");
        //console.log('Creating Associations at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.post({
                url: final_url,
                type: "POST",
                async: async,
                data: $.trim(JSON.stringify(post_data)),
                dataType: "json",
                contentType: "application/json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                var association = jQuery.parseJSON(JSON.stringify(data));
                callback(association);
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot create associations due to server error.", jqXHR));
            })
    }
}
function delete_association_call(async, sourceID, sinkID, callback) {
    // First delete this note AJAX
    var cookie_sid = get_cookie_access();
    var url = APIS.get("DELETE_ASSOCIATIONS");
    var final_url = url + "?source=" + preprocess_html_escape(sourceID) + "&sink=" + preprocess_html_escape(sinkID);
    //console.log('Deleting association at URL: ' + final_url);

    ////console.log("Deleting associations from URL: " + final_url);
    // Send an AJAX request to fetch notes
    $.ajax({
            url: final_url,
            type: "DELETE",
            async: async,
            dataType: "json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Token " + cookie_sid
            }
        })
        .done(function (data, textStatus, jqXHR) {
            callback(jQuery.parseJSON(JSON.stringify(data)));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            callback(new DigitaryWebError("Cannot delete association due to server error.", jqXHR));
        })
}

/***************************************************************
 * TASK CALLS
 ***************************************************************/
//GET TASK CALL
function get_tasks_call(data_map, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        //var startWeek = moment().utc().startOf('week');
        //var endWeek = moment().utc().endOf('week');
        var newTime = moment("2016-05-16 08:00 +0000", "YYYY-MM-DD HH:mm Z");
        var startWeek = moment(newTime).utc().startOf('week');
        var endWeek = moment(newTime).utc().endOf('week');
        var final_url = get_final_computable_url("GET_TASK", data_map);
        //var final_url = url + "?start="+startWeek.valueOf()+"&end="+endWeek.valueOf();
        //console.log('Getting tasks at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.ajax({
                url: final_url,
                type: "GET",
                async: async,
                dataType: "json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                var tasks = jQuery.parseJSON(JSON.stringify(data));
                callback(tasks);
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot retrieve task hierarchy due to server error.", jqXHR));
            })
    }
}
//UPDATE TASK CALL
function update_task_call(data_map, post_data, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("UPDATE_TASK", data_map);
        //console.log('Updating task at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.ajax({
                url: final_url,
                type: "PUT",
                async: async,
                data: $.trim(JSON.stringify(post_data)),
                dataType: "json",
                contentType: "application/json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot update task due to server error.", jqXHR));
            })
    }
}
//CREATE TASK CALL
function create_task_call(data_map, post_data, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("CREATE_TASK", data_map);
        //console.log('Creating task at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.post({
                url: final_url,
                type: "POST",
                async: async,
                data: $.trim(JSON.stringify(post_data)),
                dataType: "json",
                contentType: "application/json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot create task due to server error.", jqXHR));
            })
    }
}
//DELETE TASK CALL
function delete_task_call(data_map, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("DELETE_TASK", data_map);
        //console.log('Deleting task at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.ajax({
                url: final_url,
                type: "DELETE",
                async: async,
                dataType: "json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot delete task due to server error.", jqXHR));
            })
    }
}

/***************************************************************
 * ASSOCIATIONS
 ***************************************************************/
function get_associated_entities_call(data_map, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("GET_ASSOCIATED_ENTITIES", data_map);
        //console.log('Getting associated entities at URL: ' + final_url);
        // Send an AJAX request to fetch associations
        $.ajax({
                url: final_url,
                type: "GET",
                async: async,
                dataType: "json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));

            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot fetch associations due to server error.", jqXHR));
            })
    }
}
/***************************************************************
 * Calendar
 ***************************************************************/
function get_event_call(data_map, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = "test/data/events.json"//get_final_computable_url("GET_NOTEBOOK_HIERARCHY", data_map);
        //console.log('Getting calendar at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.ajax({
                url: final_url,
                type: "GET",
                async: async,
                dataType: "json"/*,
                 headers: {
                 "AppID": APP_ID,
                 "Authorization": "Token " + cookie_sid
                 }*/
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot retrieve task hierarchy due to server error.", jqXHR));
            })
    }
}
/***************************************************************
 * EMAIL
 ***************************************************************/
function get_email_request_auth(email, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("EMAIL_REQ_AUTH", {});
        final_url = final_url + "?uid=" + preprocess_html_escape(USR_PROFILE.id) + "&";
        final_url = final_url + "email=" + email;
        // Send an AJAX request to fetch notebook hierarchy
        $.ajax({
                url: final_url,
                type: "GET",
                async: false,
                dataType: "json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot get add email auth due to server error.", jqXHR));
            })
    }
}

function get_email_call(data_map, async, call_type, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("GET_CONVERSATIONS", data_map);
        final_url = final_url + "?type=" + call_type;
        // Send an AJAX request to fetch notebook hierarchy
        $.ajax({
                url: final_url,
                type: "GET",
                async: async,
                dataType: "json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot retrieve all emails due to server error.", jqXHR));
            })
    }
}
//CALL EMAIL SENT DATA
function get_sent_email_call(data_map, async, call_type, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("GET_SENT_CONVERSATIONS", data_map);
        final_url = final_url + "?type=" + call_type;
        // Send an AJAX request to fetch notebook hierarchy
        $.ajax({
            url: final_url,
            type: "GET",
            async: async,
            dataType: "json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Token " + cookie_sid
            }
        })
            .done(function (data, textStatus, jqXHR) {
                callback(jQuery.parseJSON(JSON.stringify(data)));
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot retrieve sent email due to server error.", jqXHR));
            })
    }
}
//Move conversation to archive
function update_conversation_archive(data_map, async, callback) {
    var cookie_sid = get_cookie_access();
    var final_url = get_final_computable_url("ARCHIVE_CONVERSATIONS", data_map);
    // Send an AJAX request to fetch notes
    $.ajax({
            url: final_url,
            type: "PUT",
            async: async,
            dataType: "json",
            contentType: "application/json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Token " + cookie_sid
            }
        })
        .done(function (data, textStatus, jqXHR) {
            callback({});
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == 200)
                callback({});
            else
                callback(new DigitaryWebError("Cannot archive conversation due to server error.", jqXHR));
        })
}
//Update conversation status
function update_conversation_status(data_map, async, callback) {
    var cookie_sid = get_cookie_access();
    var final_url = get_final_computable_url("UPDATE_CONVERSATION_STATUS", data_map);
    // Send an AJAX request to fetch notes
    $.ajax({
        url: final_url,
        type: "PUT",
        async: async,
        dataType: "json",
        contentType: "application/json",
        headers: {
            "AppID": APP_ID,
            "Authorization": "Token " + cookie_sid
        }
    })
        .done(function (data, textStatus, jqXHR) {
            callback({});
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == 200)
                callback({});
            else
                callback(new DigitaryWebError("Cannot update conversation status due to server error.", jqXHR));
        })
}
//Delete conversation call
function delete_conversation_call(data_map, async, callback) {
    // First delete this note AJAX
    var cookie_sid = get_cookie_access();
    var final_url = get_final_computable_url("DELETE_CONVERSATIONS", data_map);
    //console.log("Deleting a note from URL: " + final_url);
    // Send an AJAX request to fetch notes
    $.ajax({
            url: final_url,
            type: "DELETE",
            async: async,
            dataType: "json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Token " + cookie_sid
            }
        })
        .done(function (data, textStatus, jqXHR) {
            callback(jQuery.parseJSON(JSON.stringify(data)));
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            callback(new DigitaryWebError("Cannot delete note due to server error.", jqXHR));
        })
}
//Send Email call
function send_email_message(data_map, post_data, async, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = get_final_computable_url("SEND_EMAIL", data_map);

        // Send an AJAX request to fetch notebook hierarchy
        $.post({
            url: final_url,
            type: "POST",
            async: async,
            data: $.trim(JSON.stringify(post_data)),
            dataType: "json",
            contentType: "application/json",
            headers: {
                "AppID": APP_ID,
                "Authorization": "Token " + cookie_sid
            }
        })
         .done(function (data, textStatus, jqXHR) {
             callback({});
         })
         .fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == 200)
                 callback({});
            else
                callback(new DigitaryWebError("Cannot send email due to server error.", jqXHR));
         })
    }
}
/***************************************************************
 * SEARCH CALLS
 ***************************************************************/
//GET LABELS CALL
function get_search_call(q, type, isLabelSearch, callback) {
    var cookie_sid = get_cookie_access();
    if (cookie_sid) {
        var final_url = '';
        if (type == ENTITY_EMAIL) {
            var accountID = USR_PROFILE.getEmailAccounts()[0];

            if (accountID) {
                var map = new Map();
                map.set("{accountID}", accountID);
                final_url = get_final_computable_url("SEARCH_EMAIL", map);
            } else
                return;
        } else {
            final_url = get_final_computable_url("SEARCH");
        }

        if (!q) {
            throw new Error("Cannot search without query.");
        }
        final_url = final_url + "?q=" + q;
        if (type) {
            final_url = final_url + "&type=" + type;
        }
        //console.log('Getting labels at URL: ' + final_url);
        // Send an AJAX request to fetch notebook hierarchy
        $.ajax({
                url: final_url,
                type: "GET",
                async: true,
                dataType: "json",
                headers: {
                    "AppID": APP_ID,
                    "Authorization": "Token " + cookie_sid
                }
            })
            .done(function (data, textStatus, jqXHR) {
                var labels = jQuery.parseJSON(JSON.stringify(data));
                callback(labels);
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                callback(new DigitaryWebError("Cannot perform search due to server error.", jqXHR));
            })
    }
}


