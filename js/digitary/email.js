/**
 * Created by Digitay Development Team: Siddhartha Moitra, Nitesh Singh on 18.04.16.
 */

function decode_email_base64(encodedMessage) {
    encodedMessage = encodedMessage.replace(/-/g, '+');
    encodedMessage = encodedMessage.replace(/_/g, '/');

    //var messageHtml = CryptoJS.enc.Base64.parse(encodedMessage);
    //messageHtml = messageHtml.toString(CryptoJS.enc.Utf8);
    var messageHtml = window.atob(encodedMessage);
    return messageHtml;
}
function split_email_html_message(htmlMessage) {
    var split_using = "gmail_extra";
    var indexOf = htmlMessage.indexOf(split_using);
    indexOf = indexOf - 12;
    var splits = [];
    if(indexOf > 0) {
        splits.push(htmlMessage.substring(0, indexOf));
        splits.push(htmlMessage.substring(indexOf));
    } else {
        splits.push(htmlMessage);
    }

    return splits;
}
function get_message_from(message) {
    var from = message.from;
    var parsedFrom = from.substring(0, from.indexOf('<'));
    if(parsedFrom)
        return parsedFrom;
    else
        return from;
}

function get_conversation_from(conversation) {
    return get_message_from(conversation.messageList[conversation.messageList.length - 1]);
}
function get_conversation_subject(conversation) {
    return conversation.messageList[conversation.messageList.length - 1].subject;
}
function init_email_base(conversations) {
    $('td.details-control').on('click', function (e) {
        e.preventDefault();
        $(e.target).closest('tr').next().toggleClass('hidden');
    });
    $('div.show-trimmed-message a').on('click', function (e) {
        e.stopPropagation();
        $(e.target).toggleClass('fa-plus-square fa-minus-square');
        $(e.target).closest('div.show-trimmed-message').next().toggleClass('show hide');
    });

    $.each(conversations, function (index, conversation) {
        var conversationHtmlID = get_html_id_from_internal_id(conversation.id);
        if(conversation.unreadMessageCount>0) {
            $('#row_'+conversationHtmlID).addClass('unread');
        }
        // Associate labels to tagIt
        var tagItUlID = '#conversation-label-' + conversationHtmlID;
        $(tagItUlID).tagit({
            availableTags: USR_PROFILE.getLabels(),
            afterTagAdded: function (evt, ui) {
                if (!ui.duringInitialization) {
                    var labelName = $(tagItUlID).tagit('tagLabel', ui.tag);
                    var label = USR_PROFILE.getLabelByName(labelName);

                    var association = new Object();
                    association.sourceID = get_internal_id_from_html_id($(tagItUlID).closest('tr').attr('id'));
                    association.sinkID = label.id;
                    // Call to API to add label
                    create_association_call(association, true, function (association) {
                        if (association instanceof DigitaryWebError) {
                            show_message_on_base_modal(association);
                            show_error_message("Could not assign label.",association);
                        } else {
                            show_message_on_base_modal('<b>Added Label:</b> ' + labelName);
                            // Now add this to the outer task list table
                            var taskHtmlID = get_html_id_from_internal_id(association.sourceID);
                            var newLabelHtml = "<span class='label label-as-badge label-warning tags' onclick='javascript: load_tagged_entities_by_label_name(event);'>";
                            newLabelHtml = newLabelHtml + labelName + "</span>";
                            $('#'+taskHtmlID).children('td:nth-child(3)').append(newLabelHtml);
                            show_success_message("Successfully assigned label.");
                        }
                    });
                }
            },
            afterTagRemoved: function(evt, ui) {
                var labelName = $(tagItUlID).tagit('tagLabel', ui.tag);
                var label = USR_PROFILE.getLabelByName(labelName);

                var sourceID = get_internal_id_from_html_id($(tagItUlID).closest('tr').attr('id'));
                var sinkID = label.id;
                // Call to API to Delete label
                delete_association_call(true, sourceID, sinkID, function (association) {
                    if (association instanceof DigitaryWebError) {
                        show_message_on_base_modal(association);
                        show_error_message("Could not delete label.",association);
                    } else {
                        show_message_on_base_modal('<b>Removed Label:</b> ' + labelName);
                        var taskHtmlID = get_html_id_from_internal_id(association.sourceID);
                        $('#' + taskHtmlID).find("span:contains(" + labelName + ")").remove();
                        show_success_message("Successfully deleted label.");
                    }
                });
            }
        });
    });

}

function load_associative_editor_email(e) {
    e = e || window.event;
    var emailMessageHtmlID = $(e.target).closest('div.panel-default').children('div:nth-child(2)').attr('id');
    var emailMessageID = get_internal_id_from_html_id(emailMessageHtmlID);

    var map = new Map();
    map.set("{entityID}", emailMessageID);

    load_base_modal("Associative Editor for: <b>Email Message</b>");
    get_associated_entities_call(map, true, function (association) {
        var templateData = new Object();
        // Load the html template file
        var html_template = get_inner_HTML("templates/email/associative-editor-email.html");
        var template = _.template(html_template);

        show_data_on_base_modal(template(templateData));
        // Save the taskID in a placeholder
        $('#aet-entity').val(association.entity.id);

        templateData = new Object();
        templateData.message = association.entity;
        html_template = get_inner_HTML("templates/email/show-email.html");
        template = _.template(html_template);
        $('#email-details').append(template(templateData));

        templateData = new Object();
        templateData.noteList = association.noteList;
        html_template = get_inner_HTML("templates/common/list-note.html");
        template = _.template(html_template);
        $('#existing-note-association div.clearfix:first-child').append(template(templateData));

        templateData = new Object();
        templateData.taskList = association.taskList;
        html_template = get_inner_HTML("templates/common/list-task.html");
        template = _.template(html_template);
        $('#existing-task-association div.clearfix:first-child').append(template(templateData));
    });
}
//Create note for Email Starts
function load_note_editor_from_email(e) {
    e = e || window.event;
    var messageHtmlID = $(e.target).closest('div.panel-default').children('div:nth-child(2)').attr('id');
    var messageID = get_internal_id_from_html_id(messageHtmlID);
    var notebook = USR_PROFILE.getNotebooks()[0];

    var templateData = new Object();
    templateData.messageID = messageID;
    templateData.notebook = notebook;
    // Load the html template file
    var html_template = get_inner_HTML("templates/email/create-note-from-email.html");
    var template = _.template(html_template);
    load_base_modal("Create note");
    show_data_on_base_modal(template(templateData));
    // Associate TinyMCE editor
    associate_tinymce_editor_notinline_with_id('create-note-body-email-modal');
    // Add all event handlers for this modal
    add_event_handler_email_note_editor();
}

function add_event_handler_email_note_editor() {
    // Code to flush modal from DOM completely
    $("#base-modal").on('hidden.bs.modal', function () {
        var editorID = tinymce.get('create-note-body-email-modal');
        //alert(editorID);
        tinymce.execCommand('mceRemoveEditor', true, editorID);
    });
    if (tinymce.editors.length>0) {
        tinymce.execCommand('mceFocus', true, 'create-note-body-email-modal');
        tinymce.execCommand('mceRemoveEditor',true, 'create-note-body-email-modal');
        tinymce.execCommand('mceAddEditor',true, 'create-note-body-email-modal');
    }

    $('#select-noteBook-section').change(function () {

        var selectedSection = $(this).find(":selected").val();
        $('#select-section-page option').each(function () {
            var self = $(this);
            if (self.hasClass(selectedSection)) {
                self.addClass('show');
                self.removeClass('hide');
            } else {
                self.addClass('hide');
                self.removeClass('show');
            }
        });

    });
}

function create_note_from_email() {
    var content_html = tinyMCE.get('create-note-body-email-modal').getContent();

    // Make this MD format
    // TODO
    convert_html_to_markdown(content_html, function (markdown) {
        var notebookID = $('#selected-noteBookID').val();
        var selectedSectionID = get_internal_id_from_html_id($('#select-noteBook-section').find(":selected").val());
        var selectedPageID = get_internal_id_from_html_id($('#select-section-page').find(":selected").val());

        var onSpotNoteForMessage = new Object();
        onSpotNoteForMessage.noteBody = markdown;
        var map = new Map();
        map.set("{notebookID}", notebookID);
        map.set("{xpath}", selectedSectionID);
        map.set("{pageID}", selectedPageID);
        create_note_call(map, onSpotNoteForMessage, true, function (note) {
            if (note instanceof DigitaryWebError) {
                show_message_on_base_modal(note);
            }
            else {
                show_message_on_base_modal("<b>Successfully</b> created note.");
                var notes = [];
                notes.push(note);
                //add created note on Notebook UI
                var pageID = 'page_content_' + get_html_id_from_internal_id(selectedPageID);

                add_note($('#' + pageID).find('div.page-content'), note);

                //create association now
                var association = new Object();
                association.sourceID = $('#selected-noteMessageID').val();
                association.sinkID = note.id;
                create_association_call(association, true, function (association) {
                    if (association instanceof DigitaryWebError) {
                        show_data_on_base_modal(association);
                        show_error_message("Could not create association.",association);
                    }
                    else {
                        show_data_on_base_modal("<b>Successfully</b> associated the note to task.");
                        show_success_message("Successfully created note and associated to respective message.")
                    }
                });
            }
        });
    });
}
function remove_association_email_to_note(e) {
    var noteHtmlID = $('#existing-note-association').find('div.tab-pane.active').attr('id');
    // This value is "aet_<htmlid of note>". So we substring aet_
    noteHtmlID = noteHtmlID.substr(4);
    if(noteHtmlID) {
        var noteID = get_internal_id_from_html_id(noteHtmlID);
        var messageID = $('#aet-entity').val();

        delete_association_call(true, messageID, noteID, function (association) {
            if (association instanceof DigitaryWebError) {
                show_message_on_base_modal(association);
            } else {
                // Add this from existing associations UI
                var id = $('#existing-note-association').find('div.tab-pane.active').attr('id');
                $('#' + id).remove();
                $("a[href='#"+ id + "']").closest('tr').remove();
                // Show message
                show_message_on_base_modal("<b>Successfully</b> removed association of note to the email.");
            }
        });
    }
}
function create_association_email_to_note(e) {
    var noteHtmlID = $('#aet-note-search').find('div.tab-pane.active').attr('id');
    // This value is "aet_<htmlid of note>". So we substring aet_
    noteHtmlID = noteHtmlID.substr(4);
    if(noteHtmlID) {
        var noteID = get_internal_id_from_html_id(noteHtmlID);
        var messageID = $('#aet-entity').val();
        var association = {};
        association.sourceID = messageID;
        association.sinkID = noteID;
        create_association_call(association, true, function (association) {
            if (association instanceof DigitaryWebError) {
                show_message_on_base_modal(association);
            } else {
                // Add this to existing associations UI
                var id = $('#aet-note-search').find('div.tab-pane.active').attr('id');
                var text = $('#' + id).html();
                text = "<div class='tab-pane' id='" + id + "'>" + text + "</div>";
                $('#' + id).remove();

                var ahref = $("a[href='#"+ id + "']").closest('tr').detach();
                // Add these 2
                $('#existing-note-association').find('div.tab-content').append(text);
                ahref.appendTo('#existing-note-association table.list-note tbody');
                // Show message
                show_message_on_base_modal("<b>Successfully</b> added association of note to the email.");
            }
        });
    }
}
function create_association_email_to_task(e) {
    var taskHtmlID = $('#aet-task-search').find('div.tab-pane.active').attr('id');
    // This value is "aet_<htmlid of note>". So we substring aet_
    taskHtmlID = taskHtmlID.substr(4);
    if(taskHtmlID) {
        var taskID = get_internal_id_from_html_id(taskHtmlID);
        var messageID = $('#aet-entity').val();
        var association = {};
        association.sourceID = messageID;
        association.sinkID = taskID;
        create_association_call(association, true, function (association) {
            if (association instanceof DigitaryWebError) {
                show_message_on_base_modal(association);
            } else {
                // Add this to existing associations UI
                var id = $('#aet-task-search').find('div.tab-pane.active').attr('id');
                var text = $('#' + id).html();
                text = "<div class='tab-pane' id='" + id + "'>" + text + "</div>";
                $('#' + id).remove();

                var ahref = $("a[href='#"+ id + "']").closest('tr').detach();
                // Add these 2
                $('#existing-task-association').find('div.tab-content').append(text);
                ahref.appendTo('#existing-task-association table.list-task tbody');
                // Show message
                show_message_on_base_modal("<b>Successfully</b> added association of task to the email.");
            }
        });
    }
}
function remove_association_email_to_task(e) {
    var taskHtmlID = $('#existing-task-association').find('div.tab-pane.active').attr('id');
    // This value is "aet_<htmlid of note>". So we substring aet_
    taskHtmlID = taskHtmlID.substr(4);
    if(taskHtmlID) {
        var taskID = get_internal_id_from_html_id(taskHtmlID);
        var messageID = $('#aet-entity').val();

        delete_association_call(true, messageID, taskID, function (association) {
            if (association instanceof DigitaryWebError) {
                show_message_on_base_modal(association);
            } else {
                // Add this from existing associations UI
                var id = $('#existing-task-association').find('div.tab-pane.active').attr('id');
                $('#' + id).remove();
                $("a[href='#"+ id + "']").closest('tr').remove();
                // Show message
                show_message_on_base_modal("<b>Successfully</b> removed association of task to the email.");
            }
        });
    }
}
//Create note for Email Starts
function load_task_editor_from_email(e) {
    e = e || window.event;
    var messageHtmlID = $(e.target).closest('div.panel-default').children('div:nth-child(2)').attr('id');
    var messageID = get_internal_id_from_html_id(messageHtmlID);

    load_task_creation_for_email(messageID);
}
function load_task_creation_for_email(messageID){
    var templateData = new Object();
    templateData.messageID = messageID;
    // Load the html template file
    var html_template = get_inner_HTML("templates/email/create-task-from-email.html");
    var template = _.template(html_template);
    load_base_modal("Create task:");
    show_data_on_base_modal(template(templateData));
    $('#messageTaskDueDate').datepicker();
}

//Move conversation to archive
function move_conversation_to_archive(e){
    e = e || window.event;

    var conversationHtmlID = $(e.target).closest('tr').attr('id');
    var conversationID = get_internal_id_from_html_id(conversationHtmlID);
    var emailAccountID = USR_PROFILE.getEmailAccounts()[0];

    var map = new Map();
    map.set("{accountID}", emailAccountID);
    map.set("{conversationID}", conversationID);
    //call api to update conversation
    update_conversation_archive(map, true, function(error){
        if (error instanceof DigitaryWebError) {
            //console.log("Cannot move conversation to Archive: " + conversation.error.causeMessage);
            show_error_message("Could not move email to archive.",error);
        } else {
            $('#row_'+conversationHtmlID).next().toggleClass('hidden');
            
            var convDetlTR = $('#row_'+conversationHtmlID).next().detach();
            var convListTR = $('#row_'+conversationHtmlID).detach();

            $('#table-archive tbody').append(convListTR);
            $('#table-archive tbody').append(convDetlTR);
            show_success_message("Successfully moved email to archive.");
        }
    });
}
//Delete conversation
function delete_conversation(e){
    e = e || window.event;
    var conversationHtmlID = $(e.target).closest('tr').prev().attr('id').substr(4);
    var conversationID = get_internal_id_from_html_id(conversationHtmlID);
    var emailAccountID = USR_PROFILE.getEmailAccounts()[0];

    var map = new Map();
    map.set("{accountID}", emailAccountID);
    map.set("{conversationID}", conversationID);

    //call api to delete conversation
    delete_conversation_call(map, true, function(conversation){
        if (conversation instanceof DigitaryWebError) {
            //console.log("Cannot delete conversation: " + conversation.error.causeMessage);
            show_error_message("Could not delete email.",conversation);
        } else {
            //console.log("Conversation successfully deleted");
            //location.reload();
            $('#row_'+conversationHtmlID).remove('tr');
            $(e.target).closest('tr').remove();
            show_success_message("Successfully deleted email.");
        }
    });
}
//Read conversation
function message_status_check(e){
    e = e || window.event;
    var conversationHtmlID = $(e.target).closest('tr').attr('id').substr(4);
    var conversationID = get_internal_id_from_html_id(conversationHtmlID);
    var emailAccountID = USR_PROFILE.getEmailAccounts()[0];

    var map = new Map();
    map.set("{accountID}", emailAccountID);
    map.set("{conversationID}", conversationID);

    if ($('#row_'+conversationHtmlID).hasClass("unread")) {
        //Call Api to update conversation status
        update_conversation_status(map, true, function(conversation){
            if (conversation instanceof DigitaryWebError) {
                //console.log("Cannot update conversation status: " + conversation.error.causeMessage);
                show_error_message("Could not update conversation as read.", conversation);
            } else {
                $('#row_'+conversationHtmlID).removeClass("unread");
                //console.log("Successfully updated conversation status");
                show_success_message("Updated conversation as read.");
            }
        });
    }
}
//Send email function
function load_compose_email(e) {
    e = e || window.event;
    var templateData = new Object();
    var html_template = get_inner_HTML("templates/email/compose-mail.html");
    var template = _.template(html_template);
    load_base_modal("Compose message:");
    show_data_on_base_modal(template(templateData));
    // Associate editor with the textarea ID
    tinymce.init({
        selector: '#create-email-message',
        height: 250,
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist",
        menubar: false,
        fontsize_formats: "14pt"
    });
    if (tinymce.editors.length>0) {
        tinymce.execCommand('mceFocus', true, 'create-email-message');
        tinymce.execCommand('mceRemoveEditor',true, 'create-email-message');
        tinymce.execCommand('mceAddEditor',true, 'create-email-message');
    }
}
function send_email(e){
    e = e || window.event;
    var emailTo = $('#inputTo').val();
    var emailSubject = $('#inputSubject').val();
    var content_html = tinyMCE.get('create-email-message').getContent();

    var regex = /(<([^>]+)>)/ig;
    var resultBody = content_html.replace(regex, "");
    var encodedMessage = btoa(resultBody);
    var emailAccountID = USR_PROFILE.getEmailAccounts()[0];
    var newEmailMessage = new Object();
    newEmailMessage.subject = emailSubject;
    newEmailMessage.to = [emailTo];
    newEmailMessage.rawBody = encodedMessage;


    var map = new Map();
    map.set("{accountID}", emailAccountID);
    //Call Api to send message
    send_email_message(map, newEmailMessage, true , function(error){
        if (error instanceof DigitaryWebError) {
            //console.log("Cannot send message: " + error.error.causeMessage);
            show_error_message("Could not send email.", error);
        } else {
            //console.log("Message Successfully sent");
            show_success_message("Email sent successfully.");

        }

    });
}
//Load reply email  function
function load_reply_email(e) {
    e = e || window.event;
    var reply = $(e.target).closest('tr').children('td:first-child').children('div').find('a').text();
    var replyTo =reply.substring(reply.lastIndexOf("<")+1,reply.lastIndexOf(">"));
    var conversationHtmlID = $(e.target).closest('div.panel-group').parent().parent().parent().parent().attr('id');
    var conversationID = get_internal_id_from_html_id(conversationHtmlID);
    var replySubject = $(e.target).closest('div.panel-group').parent().parent().parent().parent().prev('tr').children('td:nth-child(2)').find('a').text().trim();
    //var replySubjectText =reply.substring(reply.lastIndexOf("+"+")+1,reply.lastIndexOf("+"+"));
    

    var templateData = new Object();
    templateData.conversationID = conversationID;
    templateData.replyMesssage = replyTo;
    templateData.subject = replySubject;
    var html_template = get_inner_HTML("templates/email/reply-mail.html");
    var template = _.template(html_template);
    load_base_modal("Reply message:");
    show_data_on_base_modal(template(templateData));
    // Associate editor with the textarea ID
    tinymce.init({
        selector: '#create-reply-email-message',
        height: 250,
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist",
        menubar: false,
        fontsize_formats: "14pt"
    });
    if (tinymce.editors.length>0) {
        tinymce.execCommand('mceFocus', true, 'create-reply-email-message');
        tinymce.execCommand('mceRemoveEditor',true, 'create-reply-email-message');
        tinymce.execCommand('mceAddEditor',true, 'create-reply-email-message');
    }

}
//Reply message
function email_message_reply(e){
    e = e || window.event;

    var conversationID = $('#emailConversationID').val();
    var replyAddress = $('#inputReplyTo').val();
    var subject = $('#inputReplySubject').val();
    var emailAccountID = USR_PROFILE.getEmailAccounts()[0];
    var content_html = tinyMCE.get('create-reply-email-message').getContent();
    var regex = /(<([^>]+)>)/ig;
    var resultBody = content_html.replace(regex, "");
    var encodedMessage = btoa(resultBody);

    var newEmailMessage = new Object();
    newEmailMessage.conversationID = conversationID;
    newEmailMessage.to = [replyAddress];
    newEmailMessage.subject = subject;
    newEmailMessage.rawBody = encodedMessage;
    var map = new Map();
    map.set("{accountID}", emailAccountID);
    //Call Api to send message

    send_email_message(map, newEmailMessage, true , function(error){
        if (error instanceof DigitaryWebError) {
            //console.log("Cannot send message: " + error.error.causeMessage);
            show_error_message("Email cannot be sent.", error);
        } else {
            //console.log("Message Successfully sent");
            show_success_message("Email sent successfully.");

        }

    });
}

function create_task_from_email() {
        var thisMessageID = $('#messageID').val();
        var taskGroups = USR_PROFILE.getTaskGroups()[0];
        var taskGroupID = taskGroups.id;
        var messageTaskTitle = $('#messageTaskTitle').val();
        var messageTaskDueDate = $('#messageTaskDueDate').val();
        var messageTaskPriority = $('#messageTaskPriority').val();

        var convertDueDate = moment(messageTaskDueDate, "MM/DD/YYYY").utc().valueOf() + MILLISECONDS_DAY - 1;

        var newMessageTask = new Object();
        newMessageTask.title = messageTaskTitle;
        newMessageTask.dueDate = convertDueDate;
        newMessageTask.priority = messageTaskPriority;

        var map = new Map();
        map.set("{taskGroupID}", taskGroupID);
        create_task_call(map, newMessageTask, false, function (task) {
            //console.log(task);
            if (task instanceof DigitaryWebError) {
                //console.log("Cannot Create Task: " + task.error.causeMessage);
                show_error_message("Could not create task.",task);
            } else {
                var taskHtmlID = task.id;
                //console.log("Task successfully added");
                var taskID = get_html_id_from_internal_id(taskHtmlID);
                var taskRow = "<tr id='" + taskID + "'><td style='color: #3B5998;'>" + get_task_priority_name(task.priority) + "</td><td>" + task.title + "</td>" +
                    "<td>" + get_js_date(task.dueDate) + "</td><td></td><td><a href='#' class='task-edit' style='padding-right: 6%;' onclick='javascript: load_associative_editor_task(event);'>" +
                    "<i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>" +
                    "<a href='#' class='create-note-forTask' style='padding-right: 6%;' onclick='javascript: load_note_editor_from_task(event);'>" +
                    "<i class='fa fa-file-text' aria-hidden='true'></i></a>" +
                    "<a href='#' class='task-delete' style='padding-right: 6%;' onclick='javascript: delete_task(event);'>" +
                    "<i class='fa fa-trash' aria-hidden='true'></i></a></td></tr>";

                $('#table-task-active').DataTable().row.add($(taskRow)).draw();
                //create association now

                var newAssociationMessageToTask = new Object();
                newAssociationMessageToTask.sourceID = thisMessageID;
                newAssociationMessageToTask.sinkID = task.id;
                newAssociationMessageToTask.createdTime = moment().utc().valueOf();
                newAssociationMessageToTask.permissionLevelID = 2;
                //console.log(newNoteToTask);
                create_association_call(newAssociationMessageToTask, true, function (association) {
                    if (association instanceof DigitaryWebError) {
                        //console.log("Cannot create note: " + association.error.causeMessage);
                        show_error_message("Could not create association.",association);
                    }
                    else {
                        show_success_message("Task successfully created and associated to email message.");
                    }
                });
            }
        });
}


function refresh_inbox() {
    load_email_data(USR_PROFILE.getEmailAccounts()[0]);
}